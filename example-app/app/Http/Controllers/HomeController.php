<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function test()
    {
        return view('test');
    }

    public function index()
    {
        return view('welcome');
    }

}
