<script defer="" nomodule="" src="/_next/static/chunks/polyfills-c67a75d1b6f99dc8.js"></script>
<script src="/_next/static/chunks/webpack-2d6c20b89449f43d.js" defer=""></script>
<script src="/_next/static/chunks/framework-2c79e2a64abdb08b.js" defer=""></script>
<script src="/_next/static/chunks/main-0ecb9ccfcb6c9b24.js" defer=""></script>
<script src="/_next/static/chunks/pages/_app-2d287c71a40187ec.js" defer=""></script>
<script src="/_next/static/chunks/16-ef3a48e2ad36f04b.js" defer=""></script>
<script src="/_next/static/chunks/893-09b2c9bc3f91774c.js" defer=""></script>
<script src="/_next/static/chunks/pages/ki-2-noi-am-anh-da-tro-thanh-thoi-quen-f401825c18fcbc0f.js" defer=""></script>
<script src="/_next/static/j6eW-cnwBFC9Aw6cYEl6p/_buildManifest.js" defer=""></script>
<script src="/_next/static/j6eW-cnwBFC9Aw6cYEl6p/_ssgManifest.js" defer=""></script>
<style
    data-href="https://fonts.googleapis.com/css2?family=Bellota&amp;family=Oooh+Baby&amp;family=Patrick+Hand&amp;family=Phudu&amp;family=WindSong:wght@500&amp;family=Arizonia&amp;family=Mynerve&amp;family=Nunito&amp;family=Big+Shoulders+Stencil+Text&amp;family=Signika&amp;family=Srisakdi&amp;family=Signika&amp;family=Allura&amp;family=Sansita+Swashed&amp;display=swap"
>
    @font-face {
        font-family: "Allura";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/allura/v19/9oRPNYsQpS4zjuAPjw.woff) format("woff");
    }

    @font-face {
        font-family: "Arizonia";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/arizonia/v19/neIIzCemt4A5qa7mv6WF.woff) format("woff");
    }

    @font-face {
        font-family: "Bellota";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/bellota/v16/MwQ2bhXl3_qEpiwAGJE.woff) format("woff");
    }

    @font-face {
        font-family: "Big Shoulders Stencil Text";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/bigshouldersstenciltext/v21/5aUV9-i2oxDMNwY3dHfW7UAt3Q453SM15wNj53bCcab2SJYLLUtk1OGR04TIGg.woff) format("woff");
    }

    @font-face {
        font-family: "Mynerve";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/mynerve/v2/P5sCzZKPdNjb4jt7xCc.woff) format("woff");
    }

    @font-face {
        font-family: "Nunito";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/nunito/v25/XRXI3I6Li01BKofiOc5wtlZ2di8HDLshRTA.woff) format("woff");
    }

    @font-face {
        font-family: "Oooh Baby";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/ooohbaby/v4/2sDcZGJWgJTT2Jf76xQDbA.woff) format("woff");
    }

    @font-face {
        font-family: "Patrick Hand";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/patrickhand/v20/LDI1apSQOAYtSuYWp8ZhfYeMWg.woff) format("woff");
    }

    @font-face {
        font-family: "Phudu";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/phudu/v1/0FlJVPSHk0ya-7OUeO_U-Lwm7PkKtWzUSA.woff) format("woff");
    }

    @font-face {
        font-family: "Sansita Swashed";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/sansitaswashed/v17/BXR8vFfZifTZgFlDDLgNkBydPKTt3pVCeYWqJnZSW7RpbTk.woff) format("woff");
    }

    @font-face {
        font-family: "Signika";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/signika/v20/vEFO2_JTCgwQ5ejvMV0O96D01E8J0tJXHJbF.woff) format("woff");
    }

    @font-face {
        font-family: "Srisakdi";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/srisakdi/v16/yMJRMIlvdpDbkB0A-jq_.woff) format("woff");
    }

    @font-face {
        font-family: "WindSong";
        font-style: normal;
        font-weight: 500;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/windsong/v8/KR1RBsyu-P-GFEW57oeNNPWx.woff) format("woff");
    }

    @font-face {
        font-family: "Allura";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/allura/v19/9oRPNYsQpS4zjuA_hAgWHNn7GfHC.woff2) format("woff2");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "Allura";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/allura/v19/9oRPNYsQpS4zjuA_hQgWHNn7GfHC.woff2) format("woff2");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "Allura";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/allura/v19/9oRPNYsQpS4zjuA_iwgWHNn7GQ.woff2) format("woff2");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: "Arizonia";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/arizonia/v19/neIIzCemt4A5qa7mv5WOFqwYUp31kXI.woff2) format("woff2");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "Arizonia";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/arizonia/v19/neIIzCemt4A5qa7mv5WPFqwYUp31kXI.woff2) format("woff2");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "Arizonia";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/arizonia/v19/neIIzCemt4A5qa7mv5WBFqwYUp31.woff2) format("woff2");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: "Bellota";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/bellota/v16/MwQ2bhXl3_qEpiwAKJFbtUk9hbF2rA.woff2) format("woff2");
        unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
    }

    @font-face {
        font-family: "Bellota";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/bellota/v16/MwQ2bhXl3_qEpiwAKJpbtUk9hbF2rA.woff2) format("woff2");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "Bellota";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/bellota/v16/MwQ2bhXl3_qEpiwAKJtbtUk9hbF2rA.woff2) format("woff2");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "Bellota";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/bellota/v16/MwQ2bhXl3_qEpiwAKJVbtUk9hbE.woff2) format("woff2");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: "Big Shoulders Stencil Text";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/bigshouldersstenciltext/v21/5aUV9-i2oxDMNwY3dHfW7UAt3Q453SM15wNj53bCcab2SJYLLUtk1OGR04T4ESXI6fUGbDMFztk.woff) format("woff");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "Big Shoulders Stencil Text";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/bigshouldersstenciltext/v21/5aUV9-i2oxDMNwY3dHfW7UAt3Q453SM15wNj53bCcab2SJYLLUtk1OGR04T4ECXI6fUGbDMFztk.woff) format("woff");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "Big Shoulders Stencil Text";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/bigshouldersstenciltext/v21/5aUV9-i2oxDMNwY3dHfW7UAt3Q453SM15wNj53bCcab2SJYLLUtk1OGR04T4HiXI6fUGbDMF.woff) format("woff");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: "Mynerve";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/mynerve/v2/P5sCzZKPdNjb4jt79CBkiL2t2dkPJA.woff2) format("woff2");
        unicode-range: U+0370-03FF;
    }

    @font-face {
        font-family: "Mynerve";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/mynerve/v2/P5sCzZKPdNjb4jt79CxkiL2t2dkPJA.woff2) format("woff2");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "Mynerve";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/mynerve/v2/P5sCzZKPdNjb4jt79C1kiL2t2dkPJA.woff2) format("woff2");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "Mynerve";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/mynerve/v2/P5sCzZKPdNjb4jt79CNkiL2t2dk.woff2) format("woff2");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: "Nunito";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/nunito/v25/XRXI3I6Li01BKofiOc5wtlZ2di8HDLshdTk3iazbXWjgevT5.woff) format("woff");
        unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
    }

    @font-face {
        font-family: "Nunito";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/nunito/v25/XRXI3I6Li01BKofiOc5wtlZ2di8HDLshdTA3iazbXWjgevT5.woff) format("woff");
        unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
    }

    @font-face {
        font-family: "Nunito";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/nunito/v25/XRXI3I6Li01BKofiOc5wtlZ2di8HDLshdTs3iazbXWjgevT5.woff) format("woff");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "Nunito";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/nunito/v25/XRXI3I6Li01BKofiOc5wtlZ2di8HDLshdTo3iazbXWjgevT5.woff) format("woff");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "Nunito";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/nunito/v25/XRXI3I6Li01BKofiOc5wtlZ2di8HDLshdTQ3iazbXWjgeg.woff) format("woff");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: "Oooh Baby";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/ooohbaby/v4/2sDcZGJWgJTT2Jf76xQzZ2W5Kb8VZBHR.woff2) format("woff2");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "Oooh Baby";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/ooohbaby/v4/2sDcZGJWgJTT2Jf76xQzZmW5Kb8VZBHR.woff2) format("woff2");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "Oooh Baby";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/ooohbaby/v4/2sDcZGJWgJTT2Jf76xQzaGW5Kb8VZA.woff2) format("woff2");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: "Patrick Hand";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/patrickhand/v20/LDI1apSQOAYtSuYWp8ZhfYe8UcLLubg58xGL.woff2) format("woff2");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "Patrick Hand";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/patrickhand/v20/LDI1apSQOAYtSuYWp8ZhfYe8UMLLubg58xGL.woff2) format("woff2");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "Patrick Hand";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/patrickhand/v20/LDI1apSQOAYtSuYWp8ZhfYe8XsLLubg58w.woff2) format("woff2");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: "Phudu";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/phudu/v1/0FlJVPSHk0ya-7OUeO_U-Lwm7PkKtWzkQQ-p7XoDg_dlLhU.woff) format("woff");
        unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
    }

    @font-face {
        font-family: "Phudu";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/phudu/v1/0FlJVPSHk0ya-7OUeO_U-Lwm7PkKtWzkQw-p7XoDg_dlLhU.woff) format("woff");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "Phudu";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/phudu/v1/0FlJVPSHk0ya-7OUeO_U-Lwm7PkKtWzkQg-p7XoDg_dlLhU.woff) format("woff");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "Phudu";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/phudu/v1/0FlJVPSHk0ya-7OUeO_U-Lwm7PkKtWzkTA-p7XoDg_dl.woff) format("woff");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: "Sansita Swashed";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/sansitaswashed/v17/BXR8vFfZifTZgFlDDLgNkBydPKTt3pVCeYWqJnZSW7RpXTIffTuBe4Vt8Fhn.woff) format("woff");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "Sansita Swashed";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/sansitaswashed/v17/BXR8vFfZifTZgFlDDLgNkBydPKTt3pVCeYWqJnZSW7RpXTMffTuBe4Vt8Fhn.woff) format("woff");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "Sansita Swashed";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/sansitaswashed/v17/BXR8vFfZifTZgFlDDLgNkBydPKTt3pVCeYWqJnZSW7RpXT0ffTuBe4Vt8A.woff) format("woff");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: "Signika";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/signika/v20/vEFO2_JTCgwQ5ejvMV0O96D01E8J0tJXHKbOjMj-ebe0GlvFSA.woff) format("woff");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "Signika";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/signika/v20/vEFO2_JTCgwQ5ejvMV0O96D01E8J0tJXHKbPjMj-ebe0GlvFSA.woff) format("woff");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "Signika";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/signika/v20/vEFO2_JTCgwQ5ejvMV0O96D01E8J0tJXHKbBjMj-ebe0Gls.woff) format("woff");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: "Srisakdi";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/srisakdi/v16/yMJRMIlvdpDbkB0A-gqvdy1biN15Ylg.woff2) format("woff2");
        unicode-range: U+0E01-0E5B, U+200C-200D, U+25CC;
    }

    @font-face {
        font-family: "Srisakdi";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/srisakdi/v16/yMJRMIlvdpDbkB0A-gq0dy1biN15Ylg.woff2) format("woff2");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "Srisakdi";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/srisakdi/v16/yMJRMIlvdpDbkB0A-gq1dy1biN15Ylg.woff2) format("woff2");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "Srisakdi";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/srisakdi/v16/yMJRMIlvdpDbkB0A-gq7dy1biN15.woff2) format("woff2");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: "WindSong";
        font-style: normal;
        font-weight: 500;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/windsong/v8/KR1RBsyu-P-GFEW57oeNNMW6nyzcjkXn0xo.woff2) format("woff2");
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: "WindSong";
        font-style: normal;
        font-weight: 500;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/windsong/v8/KR1RBsyu-P-GFEW57oeNNMW7nyzcjkXn0xo.woff2) format("woff2");
        unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: "WindSong";
        font-style: normal;
        font-weight: 500;
        font-display: swap;
        src: url(https://fonts.gstatic.com/s/windsong/v8/KR1RBsyu-P-GFEW57oeNNMW1nyzcjkXn.woff2) format("woff2");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }
</style>
<style data-emotion="css-global" data-s="">
    :host,
    :root,
    [data-theme] {
        --chakra-ring-inset: var(--chakra-empty, /*!*/ /*!*/);
        --chakra-ring-offset-width: 0px;
        --chakra-ring-offset-color: #fff;
        --chakra-ring-color: rgba(66, 153, 225, 0.6);
        --chakra-ring-offset-shadow: 0 0 #0000;
        --chakra-ring-shadow: 0 0 #0000;
        --chakra-space-x-reverse: 0;
        --chakra-space-y-reverse: 0;
        --chakra-colors-transparent: transparent;
        --chakra-colors-current: currentColor;
        --chakra-colors-black: #000000;
        --chakra-colors-white: #ffffff;
        --chakra-colors-whiteAlpha-50: rgba(255, 255, 255, 0.04);
        --chakra-colors-whiteAlpha-100: rgba(255, 255, 255, 0.06);
        --chakra-colors-whiteAlpha-200: rgba(255, 255, 255, 0.08);
        --chakra-colors-whiteAlpha-300: rgba(255, 255, 255, 0.16);
        --chakra-colors-whiteAlpha-400: rgba(255, 255, 255, 0.24);
        --chakra-colors-whiteAlpha-500: rgba(255, 255, 255, 0.36);
        --chakra-colors-whiteAlpha-600: rgba(255, 255, 255, 0.48);
        --chakra-colors-whiteAlpha-700: rgba(255, 255, 255, 0.64);
        --chakra-colors-whiteAlpha-800: rgba(255, 255, 255, 0.8);
        --chakra-colors-whiteAlpha-900: rgba(255, 255, 255, 0.92);
        --chakra-colors-blackAlpha-50: rgba(0, 0, 0, 0.04);
        --chakra-colors-blackAlpha-100: rgba(0, 0, 0, 0.06);
        --chakra-colors-blackAlpha-200: rgba(0, 0, 0, 0.08);
        --chakra-colors-blackAlpha-300: rgba(0, 0, 0, 0.16);
        --chakra-colors-blackAlpha-400: rgba(0, 0, 0, 0.24);
        --chakra-colors-blackAlpha-500: rgba(0, 0, 0, 0.36);
        --chakra-colors-blackAlpha-600: rgba(0, 0, 0, 0.48);
        --chakra-colors-blackAlpha-700: rgba(0, 0, 0, 0.64);
        --chakra-colors-blackAlpha-800: rgba(0, 0, 0, 0.8);
        --chakra-colors-blackAlpha-900: rgba(0, 0, 0, 0.92);
        --chakra-colors-gray-50: #f7fafc;
        --chakra-colors-gray-100: #edf2f7;
        --chakra-colors-gray-200: #e2e8f0;
        --chakra-colors-gray-300: #cbd5e0;
        --chakra-colors-gray-400: #a0aec0;
        --chakra-colors-gray-500: #718096;
        --chakra-colors-gray-600: #4a5568;
        --chakra-colors-gray-700: #2d3748;
        --chakra-colors-gray-800: #1a202c;
        --chakra-colors-gray-900: #171923;
        --chakra-colors-red-50: #fff5f5;
        --chakra-colors-red-100: #fed7d7;
        --chakra-colors-red-200: #feb2b2;
        --chakra-colors-red-300: #fc8181;
        --chakra-colors-red-400: #f56565;
        --chakra-colors-red-500: #e53e3e;
        --chakra-colors-red-600: #c53030;
        --chakra-colors-red-700: #9b2c2c;
        --chakra-colors-red-800: #822727;
        --chakra-colors-red-900: #63171b;
        --chakra-colors-orange-50: #fffaf0;
        --chakra-colors-orange-100: #feebc8;
        --chakra-colors-orange-200: #fbd38d;
        --chakra-colors-orange-300: #f6ad55;
        --chakra-colors-orange-400: #ed8936;
        --chakra-colors-orange-500: #dd6b20;
        --chakra-colors-orange-600: #c05621;
        --chakra-colors-orange-700: #9c4221;
        --chakra-colors-orange-800: #7b341e;
        --chakra-colors-orange-900: #652b19;
        --chakra-colors-yellow-50: #fffff0;
        --chakra-colors-yellow-100: #fefcbf;
        --chakra-colors-yellow-200: #faf089;
        --chakra-colors-yellow-300: #f6e05e;
        --chakra-colors-yellow-400: #ecc94b;
        --chakra-colors-yellow-500: #d69e2e;
        --chakra-colors-yellow-600: #b7791f;
        --chakra-colors-yellow-700: #975a16;
        --chakra-colors-yellow-800: #744210;
        --chakra-colors-yellow-900: #5f370e;
        --chakra-colors-green-50: #f0fff4;
        --chakra-colors-green-100: #c6f6d5;
        --chakra-colors-green-200: #9ae6b4;
        --chakra-colors-green-300: #68d391;
        --chakra-colors-green-400: #48bb78;
        --chakra-colors-green-500: #38a169;
        --chakra-colors-green-600: #2f855a;
        --chakra-colors-green-700: #276749;
        --chakra-colors-green-800: #22543d;
        --chakra-colors-green-900: #1c4532;
        --chakra-colors-teal-50: #e6fffa;
        --chakra-colors-teal-100: #b2f5ea;
        --chakra-colors-teal-200: #81e6d9;
        --chakra-colors-teal-300: #4fd1c5;
        --chakra-colors-teal-400: #38b2ac;
        --chakra-colors-teal-500: #319795;
        --chakra-colors-teal-600: #2c7a7b;
        --chakra-colors-teal-700: #285e61;
        --chakra-colors-teal-800: #234e52;
        --chakra-colors-teal-900: #1d4044;
        --chakra-colors-blue-50: #ebf8ff;
        --chakra-colors-blue-100: #bee3f8;
        --chakra-colors-blue-200: #90cdf4;
        --chakra-colors-blue-300: #63b3ed;
        --chakra-colors-blue-400: #4299e1;
        --chakra-colors-blue-500: #3182ce;
        --chakra-colors-blue-600: #2b6cb0;
        --chakra-colors-blue-700: #2c5282;
        --chakra-colors-blue-800: #2a4365;
        --chakra-colors-blue-900: #1a365d;
        --chakra-colors-cyan-50: #edfdfd;
        --chakra-colors-cyan-100: #c4f1f9;
        --chakra-colors-cyan-200: #9decf9;
        --chakra-colors-cyan-300: #76e4f7;
        --chakra-colors-cyan-400: #0bc5ea;
        --chakra-colors-cyan-500: #00b5d8;
        --chakra-colors-cyan-600: #00a3c4;
        --chakra-colors-cyan-700: #0987a0;
        --chakra-colors-cyan-800: #086f83;
        --chakra-colors-cyan-900: #065666;
        --chakra-colors-purple-50: #faf5ff;
        --chakra-colors-purple-100: #e9d8fd;
        --chakra-colors-purple-200: #d6bcfa;
        --chakra-colors-purple-300: #b794f4;
        --chakra-colors-purple-400: #9f7aea;
        --chakra-colors-purple-500: #805ad5;
        --chakra-colors-purple-600: #6b46c1;
        --chakra-colors-purple-700: #553c9a;
        --chakra-colors-purple-800: #44337a;
        --chakra-colors-purple-900: #322659;
        --chakra-colors-pink-50: #fff5f7;
        --chakra-colors-pink-100: #fed7e2;
        --chakra-colors-pink-200: #fbb6ce;
        --chakra-colors-pink-300: #f687b3;
        --chakra-colors-pink-400: #ed64a6;
        --chakra-colors-pink-500: #d53f8c;
        --chakra-colors-pink-600: #b83280;
        --chakra-colors-pink-700: #97266d;
        --chakra-colors-pink-800: #702459;
        --chakra-colors-pink-900: #521b41;
        --chakra-colors-linkedin-50: #e8f4f9;
        --chakra-colors-linkedin-100: #cfedfb;
        --chakra-colors-linkedin-200: #9bdaf3;
        --chakra-colors-linkedin-300: #68c7ec;
        --chakra-colors-linkedin-400: #34b3e4;
        --chakra-colors-linkedin-500: #00a0dc;
        --chakra-colors-linkedin-600: #008cc9;
        --chakra-colors-linkedin-700: #0077b5;
        --chakra-colors-linkedin-800: #005e93;
        --chakra-colors-linkedin-900: #004471;
        --chakra-colors-facebook-50: #e8f4f9;
        --chakra-colors-facebook-100: #d9dee9;
        --chakra-colors-facebook-200: #b7c2da;
        --chakra-colors-facebook-300: #6482c0;
        --chakra-colors-facebook-400: #4267b2;
        --chakra-colors-facebook-500: #385898;
        --chakra-colors-facebook-600: #314e89;
        --chakra-colors-facebook-700: #29487d;
        --chakra-colors-facebook-800: #223b67;
        --chakra-colors-facebook-900: #1e355b;
        --chakra-colors-messenger-50: #d0e6ff;
        --chakra-colors-messenger-100: #b9daff;
        --chakra-colors-messenger-200: #a2cdff;
        --chakra-colors-messenger-300: #7ab8ff;
        --chakra-colors-messenger-400: #2e90ff;
        --chakra-colors-messenger-500: #0078ff;
        --chakra-colors-messenger-600: #0063d1;
        --chakra-colors-messenger-700: #0052ac;
        --chakra-colors-messenger-800: #003c7e;
        --chakra-colors-messenger-900: #002c5c;
        --chakra-colors-whatsapp-50: #dffeec;
        --chakra-colors-whatsapp-100: #b9f5d0;
        --chakra-colors-whatsapp-200: #90edb3;
        --chakra-colors-whatsapp-300: #65e495;
        --chakra-colors-whatsapp-400: #3cdd78;
        --chakra-colors-whatsapp-500: #22c35e;
        --chakra-colors-whatsapp-600: #179848;
        --chakra-colors-whatsapp-700: #0c6c33;
        --chakra-colors-whatsapp-800: #01421c;
        --chakra-colors-whatsapp-900: #001803;
        --chakra-colors-twitter-50: #e5f4fd;
        --chakra-colors-twitter-100: #c8e9fb;
        --chakra-colors-twitter-200: #a8dcfa;
        --chakra-colors-twitter-300: #83cdf7;
        --chakra-colors-twitter-400: #57bbf5;
        --chakra-colors-twitter-500: #1da1f2;
        --chakra-colors-twitter-600: #1a94da;
        --chakra-colors-twitter-700: #1681bf;
        --chakra-colors-twitter-800: #136b9e;
        --chakra-colors-twitter-900: #0d4d71;
        --chakra-colors-telegram-50: #e3f2f9;
        --chakra-colors-telegram-100: #c5e4f3;
        --chakra-colors-telegram-200: #a2d4ec;
        --chakra-colors-telegram-300: #7ac1e4;
        --chakra-colors-telegram-400: #47a9da;
        --chakra-colors-telegram-500: #0088cc;
        --chakra-colors-telegram-600: #007ab8;
        --chakra-colors-telegram-700: #006ba1;
        --chakra-colors-telegram-800: #005885;
        --chakra-colors-telegram-900: #003f5e;
        --chakra-borders-none: 0;
        --chakra-borders-1px: 1px solid;
        --chakra-borders-2px: 2px solid;
        --chakra-borders-4px: 4px solid;
        --chakra-borders-8px: 8px solid;
        --chakra-fonts-heading: Srisakdi, sans-serif;
        --chakra-fonts-body: Nunito, sans-serif;
        --chakra-fonts-mono: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
        --chakra-fontSizes-3xs: 0.45rem;
        --chakra-fontSizes-2xs: 0.625rem;
        --chakra-fontSizes-xs: 0.75rem;
        --chakra-fontSizes-sm: 0.875rem;
        --chakra-fontSizes-md: 1rem;
        --chakra-fontSizes-lg: 1.125rem;
        --chakra-fontSizes-xl: 1.25rem;
        --chakra-fontSizes-2xl: 1.5rem;
        --chakra-fontSizes-3xl: 1.875rem;
        --chakra-fontSizes-4xl: 2.25rem;
        --chakra-fontSizes-5xl: 3rem;
        --chakra-fontSizes-6xl: 3.75rem;
        --chakra-fontSizes-7xl: 4.5rem;
        --chakra-fontSizes-8xl: 6rem;
        --chakra-fontSizes-9xl: 8rem;
        --chakra-fontWeights-hairline: 100;
        --chakra-fontWeights-thin: 200;
        --chakra-fontWeights-light: 300;
        --chakra-fontWeights-normal: 400;
        --chakra-fontWeights-medium: 500;
        --chakra-fontWeights-semibold: 600;
        --chakra-fontWeights-bold: 700;
        --chakra-fontWeights-extrabold: 800;
        --chakra-fontWeights-black: 900;
        --chakra-letterSpacings-tighter: -0.05em;
        --chakra-letterSpacings-tight: -0.025em;
        --chakra-letterSpacings-normal: 0;
        --chakra-letterSpacings-wide: 0.025em;
        --chakra-letterSpacings-wider: 0.05em;
        --chakra-letterSpacings-widest: 0.1em;
        --chakra-lineHeights-3: 0.75rem;
        --chakra-lineHeights-4: 1rem;
        --chakra-lineHeights-5: 1.25rem;
        --chakra-lineHeights-6: 1.5rem;
        --chakra-lineHeights-7: 1.75rem;
        --chakra-lineHeights-8: 2rem;
        --chakra-lineHeights-9: 2.25rem;
        --chakra-lineHeights-10: 2.5rem;
        --chakra-lineHeights-normal: normal;
        --chakra-lineHeights-none: 1;
        --chakra-lineHeights-shorter: 1.25;
        --chakra-lineHeights-short: 1.375;
        --chakra-lineHeights-base: 1.5;
        --chakra-lineHeights-tall: 1.625;
        --chakra-lineHeights-taller: 2;
        --chakra-radii-none: 0;
        --chakra-radii-sm: 0.125rem;
        --chakra-radii-base: 0.25rem;
        --chakra-radii-md: 0.375rem;
        --chakra-radii-lg: 0.5rem;
        --chakra-radii-xl: 0.75rem;
        --chakra-radii-2xl: 1rem;
        --chakra-radii-3xl: 1.5rem;
        --chakra-radii-full: 9999px;
        --chakra-space-1: 0.25rem;
        --chakra-space-2: 0.5rem;
        --chakra-space-3: 0.75rem;
        --chakra-space-4: 1rem;
        --chakra-space-5: 1.25rem;
        --chakra-space-6: 1.5rem;
        --chakra-space-7: 1.75rem;
        --chakra-space-8: 2rem;
        --chakra-space-9: 2.25rem;
        --chakra-space-10: 2.5rem;
        --chakra-space-12: 3rem;
        --chakra-space-14: 3.5rem;
        --chakra-space-16: 4rem;
        --chakra-space-20: 5rem;
        --chakra-space-24: 6rem;
        --chakra-space-28: 7rem;
        --chakra-space-32: 8rem;
        --chakra-space-36: 9rem;
        --chakra-space-40: 10rem;
        --chakra-space-44: 11rem;
        --chakra-space-48: 12rem;
        --chakra-space-52: 13rem;
        --chakra-space-56: 14rem;
        --chakra-space-60: 15rem;
        --chakra-space-64: 16rem;
        --chakra-space-72: 18rem;
        --chakra-space-80: 20rem;
        --chakra-space-96: 24rem;
        --chakra-space-px: 1px;
        --chakra-space-0-5: 0.125rem;
        --chakra-space-1-5: 0.375rem;
        --chakra-space-2-5: 0.625rem;
        --chakra-space-3-5: 0.875rem;
        --chakra-shadows-xs: 0 0 0 1px rgba(0, 0, 0, 0.05);
        --chakra-shadows-sm: 0 1px 2px 0 rgba(0, 0, 0, 0.05);
        --chakra-shadows-base: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);
        --chakra-shadows-md: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);
        --chakra-shadows-lg: 0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05);
        --chakra-shadows-xl: 0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04);
        --chakra-shadows-2xl: 0 25px 50px -12px rgba(0, 0, 0, 0.25);
        --chakra-shadows-outline: 0 0 0 3px rgba(66, 153, 225, 0.6);
        --chakra-shadows-inner: inset 0 2px 4px 0 rgba(0, 0, 0, 0.06);
        --chakra-shadows-none: none;
        --chakra-shadows-dark-lg: rgba(0, 0, 0, 0.1) 0px 0px 0px 1px, rgba(0, 0, 0, 0.2) 0px 5px 10px, rgba(0, 0, 0, 0.4) 0px 15px 40px;
        --chakra-sizes-1: 0.25rem;
        --chakra-sizes-2: 0.5rem;
        --chakra-sizes-3: 0.75rem;
        --chakra-sizes-4: 1rem;
        --chakra-sizes-5: 1.25rem;
        --chakra-sizes-6: 1.5rem;
        --chakra-sizes-7: 1.75rem;
        --chakra-sizes-8: 2rem;
        --chakra-sizes-9: 2.25rem;
        --chakra-sizes-10: 2.5rem;
        --chakra-sizes-12: 3rem;
        --chakra-sizes-14: 3.5rem;
        --chakra-sizes-16: 4rem;
        --chakra-sizes-20: 5rem;
        --chakra-sizes-24: 6rem;
        --chakra-sizes-28: 7rem;
        --chakra-sizes-32: 8rem;
        --chakra-sizes-36: 9rem;
        --chakra-sizes-40: 10rem;
        --chakra-sizes-44: 11rem;
        --chakra-sizes-48: 12rem;
        --chakra-sizes-52: 13rem;
        --chakra-sizes-56: 14rem;
        --chakra-sizes-60: 15rem;
        --chakra-sizes-64: 16rem;
        --chakra-sizes-72: 18rem;
        --chakra-sizes-80: 20rem;
        --chakra-sizes-96: 24rem;
        --chakra-sizes-px: 1px;
        --chakra-sizes-0-5: 0.125rem;
        --chakra-sizes-1-5: 0.375rem;
        --chakra-sizes-2-5: 0.625rem;
        --chakra-sizes-3-5: 0.875rem;
        --chakra-sizes-max: max-content;
        --chakra-sizes-min: min-content;
        --chakra-sizes-full: 100%;
        --chakra-sizes-3xs: 14rem;
        --chakra-sizes-2xs: 16rem;
        --chakra-sizes-xs: 20rem;
        --chakra-sizes-sm: 24rem;
        --chakra-sizes-md: 28rem;
        --chakra-sizes-lg: 32rem;
        --chakra-sizes-xl: 36rem;
        --chakra-sizes-2xl: 42rem;
        --chakra-sizes-3xl: 48rem;
        --chakra-sizes-4xl: 56rem;
        --chakra-sizes-5xl: 64rem;
        --chakra-sizes-6xl: 72rem;
        --chakra-sizes-7xl: 80rem;
        --chakra-sizes-8xl: 90rem;
        --chakra-sizes-prose: 60ch;
        --chakra-sizes-container-sm: 640px;
        --chakra-sizes-container-md: 768px;
        --chakra-sizes-container-lg: 1024px;
        --chakra-sizes-container-xl: 1280px;
        --chakra-zIndices-hide: -1;
        --chakra-zIndices-auto: auto;
        --chakra-zIndices-base: 0;
        --chakra-zIndices-docked: 10;
        --chakra-zIndices-dropdown: 1000;
        --chakra-zIndices-sticky: 1100;
        --chakra-zIndices-banner: 1200;
        --chakra-zIndices-overlay: 1300;
        --chakra-zIndices-modal: 1400;
        --chakra-zIndices-popover: 1500;
        --chakra-zIndices-skipLink: 1600;
        --chakra-zIndices-toast: 1700;
        --chakra-zIndices-tooltip: 1800;
        --chakra-transition-property-common: background-color, border-color, color, fill, stroke, opacity, box-shadow, transform;
        --chakra-transition-property-colors: background-color, border-color, color, fill, stroke;
        --chakra-transition-property-dimensions: width, height;
        --chakra-transition-property-position: left, right, top, bottom;
        --chakra-transition-property-background: background-color, background-image, background-position;
        --chakra-transition-easing-ease-in: cubic-bezier(0.4, 0, 1, 1);
        --chakra-transition-easing-ease-out: cubic-bezier(0, 0, 0.2, 1);
        --chakra-transition-easing-ease-in-out: cubic-bezier(0.4, 0, 0.2, 1);
        --chakra-transition-duration-ultra-fast: 50ms;
        --chakra-transition-duration-faster: 100ms;
        --chakra-transition-duration-fast: 150ms;
        --chakra-transition-duration-normal: 200ms;
        --chakra-transition-duration-slow: 300ms;
        --chakra-transition-duration-slower: 400ms;
        --chakra-transition-duration-ultra-slow: 500ms;
        --chakra-blur-none: 0;
        --chakra-blur-sm: 4px;
        --chakra-blur-base: 8px;
        --chakra-blur-md: 12px;
        --chakra-blur-lg: 16px;
        --chakra-blur-xl: 24px;
        --chakra-blur-2xl: 40px;
        --chakra-blur-3xl: 64px;
        --chakra-breakpoints-base: 0em;
        --chakra-breakpoints-sm: 30em;
        --chakra-breakpoints-md: 48em;
        --chakra-breakpoints-lg: 62em;
        --chakra-breakpoints-xl: 80em;
        --chakra-breakpoints-2xl: 96em;
    }

    .chakra-ui-light :host:not([data-theme]),
    .chakra-ui-light :root:not([data-theme]),
    .chakra-ui-light [data-theme]:not([data-theme]),
    [data-theme="light"] :host:not([data-theme]),
    [data-theme="light"] :root:not([data-theme]),
    [data-theme="light"] [data-theme]:not([data-theme]),
    :host[data-theme="light"],
    :root[data-theme="light"],
    [data-theme][data-theme="light"] {
        --chakra-colors-chakra-body-text: var(--chakra-colors-gray-800);
        --chakra-colors-chakra-body-bg: var(--chakra-colors-white);
        --chakra-colors-chakra-border-color: var(--chakra-colors-gray-200);
        --chakra-colors-chakra-subtle-bg: var(--chakra-colors-gray-100);
        --chakra-colors-chakra-placeholder-color: var(--chakra-colors-gray-500);
    }

    .chakra-ui-dark :host:not([data-theme]),
    .chakra-ui-dark :root:not([data-theme]),
    .chakra-ui-dark [data-theme]:not([data-theme]),
    [data-theme="dark"] :host:not([data-theme]),
    [data-theme="dark"] :root:not([data-theme]),
    [data-theme="dark"] [data-theme]:not([data-theme]),
    :host[data-theme="dark"],
    :root[data-theme="dark"],
    [data-theme][data-theme="dark"] {
        --chakra-colors-chakra-body-text: var(--chakra-colors-whiteAlpha-900);
        --chakra-colors-chakra-body-bg: var(--chakra-colors-gray-800);
        --chakra-colors-chakra-border-color: var(--chakra-colors-whiteAlpha-300);
        --chakra-colors-chakra-subtle-bg: var(--chakra-colors-gray-700);
        --chakra-colors-chakra-placeholder-color: var(--chakra-colors-whiteAlpha-400);
    }
</style>
<style data-emotion="css-global" data-s="">
    html {
        line-height: 1.5;
        -webkit-text-size-adjust: 100%;
        font-family: system-ui, sans-serif;
        -webkit-font-smoothing: antialiased;
        text-rendering: optimizeLegibility;
        -moz-osx-font-smoothing: grayscale;
        touch-action: manipulation;
    }

    body {
        position: relative;
        min-height: 100%;
        margin: 0;
        font-feature-settings: "kern";
    }

    :where(*, *::before, *::after) {
        border-width: 0;
        border-style: solid;
        box-sizing: border-box;
        word-wrap: break-word;
    }

    main {
        display: block;
    }

    hr {
        border-top-width: 1px;
        box-sizing: content-box;
        height: 0;
        overflow: visible;
    }

    :where(pre, code, kbd, samp) {
        font-family: SFMono-Regular, Menlo, Monaco, Consolas, monospace;
        font-size: 1em;
    }

    a {
        background-color: transparent;
        color: inherit;
        -webkit-text-decoration: inherit;
        text-decoration: inherit;
    }

    abbr[title] {
        border-bottom: none;
        -webkit-text-decoration: underline;
        text-decoration: underline;
        -webkit-text-decoration: underline dotted;
        -webkit-text-decoration: underline dotted;
        text-decoration: underline dotted;
    }

    :where(b, strong) {
        font-weight: bold;
    }

    small {
        font-size: 80%;
    }

    :where(sub, sup) {
        font-size: 75%;
        line-height: 0;
        position: relative;
        vertical-align: baseline;
    }

    sub {
        bottom: -0.25em;
    }

    sup {
        top: -0.5em;
    }

    img {
        border-style: none;
    }

    :where(button, input, optgroup, select, textarea) {
        font-family: inherit;
        font-size: 100%;
        line-height: 1.15;
        margin: 0;
    }

    :where(button, input) {
        overflow: visible;
    }

    :where(button, select) {
        text-transform: none;
    }

    :where(button::-moz-focus-inner, [type="button"]::-moz-focus-inner, [type="reset"]::-moz-focus-inner, [type="submit"]::-moz-focus-inner) {
        border-style: none;
        padding: 0;
    }

    fieldset {
        padding: 0.35em 0.75em 0.625em;
    }

    legend {
        box-sizing: border-box;
        color: inherit;
        display: table;
        max-width: 100%;
        padding: 0;
        white-space: normal;
    }

    progress {
        vertical-align: baseline;
    }

    textarea {
        overflow: auto;
    }

    :where([type="checkbox"], [type="radio"]) {
        box-sizing: border-box;
        padding: 0;
    }

    :where([type="number"]::-webkit-inner-spin-button, [type="number"]::-webkit-outer-spin-button) {
        -webkit-appearance: none !important;
    }

    input[type="number"] {
        -moz-appearance: textfield;
    }

    [type="search"] {
        -webkit-appearance: textfield;
        outline-offset: -2px;
    }

    [type="search"]::-webkit-search-decoration {
        -webkit-appearance: none !important;
    }

    ::-webkit-file-upload-button {
        -webkit-appearance: button;
        font: inherit;
    }

    details {
        display: block;
    }

    summary {
        display: -webkit-box;
        display: -webkit-list-item;
        display: -ms-list-itembox;
        display: list-item;
    }

    template {
        display: none;
    }

    [hidden] {
        display: none !important;
    }

    :where(blockquote, dl, dd, h1, h2, h3, h4, h5, h6, hr, figure, p, pre) {
        margin: 0;
    }

    button {
        background: transparent;
        padding: 0;
    }

    fieldset {
        margin: 0;
        padding: 0;
    }

    :where(ol, ul) {
        margin: 0;
        padding: 0;
    }

    textarea {
        resize: vertical;
    }

    :where(button, [role="button"]) {
        cursor: pointer;
    }

    button::-moz-focus-inner {
        border: 0 !important;
    }

    table {
        border-collapse: collapse;
    }

    :where(h1, h2, h3, h4, h5, h6) {
        font-size: inherit;
        font-weight: inherit;
    }

    :where(button, input, optgroup, select, textarea) {
        padding: 0;
        line-height: inherit;
        color: inherit;
    }

    :where(img, svg, video, canvas, audio, iframe, embed, object) {
        display: block;
    }

    :where(img, video) {
        max-width: 100%;
        height: auto;
    }

    [data-js-focus-visible] :focus:not([data-focus-visible-added]):not([data-focus-visible-disabled]) {
        outline: none;
        box-shadow: none;
    }

    select::-ms-expand {
        display: none;
    }

    :root,
    :host {
        --chakra-vh: 100vh;
    }

    @supports (height: -webkit-fill-available) {
        :root,
        :host {
            --chakra-vh: -webkit-fill-available;
        }
    }

    @supports (height: -moz-fill-available) {
        :root,
        :host {
            --chakra-vh: -moz-fill-available;
        }
    }

    @supports (height: 100dvh) {
        :root,
        :host {
            --chakra-vh: 100dvh;
        }
    }
</style>
<style data-emotion="css-global" data-s="">
    body {
        font-family: Nunito, sans-serif;
        color: var(--chakra-colors-chakra-body-text);
        background: var(--chakra-colors-chakra-body-bg);
        transition-property: background-color;
        transition-duration: var(--chakra-transition-duration-normal);
        line-height: var(--chakra-lineHeights-base);
    }

    *::-webkit-input-placeholder {
        color: var(--chakra-colors-chakra-placeholder-color);
    }

    *::-moz-placeholder {
        color: var(--chakra-colors-chakra-placeholder-color);
    }

    *:-ms-input-placeholder {
        color: var(--chakra-colors-chakra-placeholder-color);
    }

    *::placeholder {
        color: var(--chakra-colors-chakra-placeholder-color);
    }

    *,
    *::before,
    ::after {
        border-color: var(--chakra-colors-chakra-border-color);
    }
</style>
<style data-emotion="css 13f5ahd" data-s="">
    .css-13f5ahd {
        color: #ddd;
    }
</style>
<style data-emotion="css m98nha" data-s="">
    .css-m98nha {
        height: calc(100vw / 1.4998169168802635);
        width: 100vw;
    }
</style>
<style data-emotion="css xdz6mv" data-s="">
    .css-xdz6mv {
        background-image: linear-gradient(to bottom, #020c17 1200px, rgba(0, 0, 0, 0.98) 50%, #020c17 100%);
        padding-bottom: 200px;
        padding-top: 50px;
        position: relative;
        z-index: 2;
        box-shadow: #000 0px 8px 40px;
    }
</style>
<style data-emotion="css k1dh30" data-s="">
    .css-k1dh30 {
        text-align: center;
        max-width: 900px;
        z-index: 1;
        margin: 0 auto;
        -webkit-padding-start: var(--chakra-space-6);
        padding-inline-start: var(--chakra-space-6);
        -webkit-padding-end: var(--chakra-space-6);
        padding-inline-end: var(--chakra-space-6);
    }
</style>
<style data-emotion="css 1dpqnf9" data-s="">
    .css-1dpqnf9 {
        font-weight: var(--chakra-fontWeights-bold);
        line-height: 1.33;
        text-align: center;
        font-size: 60px;
        font-family: Big Shoulders Stencil Text;
    }

    @media screen and (min-width: 48em) {
        .css-1dpqnf9 {
            line-height: 1.2;
        }
    }
</style>
<style data-emotion="css hvet4e" data-s="">
    .css-hvet4e {
        font-weight: var(--chakra-fontWeights-bold);
        line-height: 1.33;
        text-align: center;
        font-size: 40px;
        font-family: Signika;
        max-width: 800px;
        margin: 0 auto;
        margin-top: var(--chakra-space-12);
    }

    @media screen and (min-width: 48em) {
        .css-hvet4e {
            line-height: 1.2;
        }
    }
</style>
<style data-emotion="css 1xndcul" data-s="">
    .css-1xndcul {
        margin-top: var(--chakra-space-6);
        font-style: italic;
        font-weight: var(--chakra-fontWeights-bold);
    }
</style>
<style data-emotion="css 10nqdgw" data-s="">
    .css-10nqdgw {
        width: 100vw;
        object-fit: cover;
    }
</style>
<style data-emotion="css bpatxa" data-s="">
    .css-bpatxa {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        max-width: 900px;
        position: relative;
        z-index: 1;
        margin: 0 auto;
        text-align: justify;
        -webkit-padding-start: var(--chakra-space-6);
        padding-inline-start: var(--chakra-space-6);
        -webkit-padding-end: var(--chakra-space-6);
        padding-inline-end: var(--chakra-space-6);
    }

    .css-bpatxa > *:not(style) ~ *:not(style) {
        margin-top: var(--chakra-space-4);
        -webkit-margin-end: 0px;
        margin-inline-end: 0px;
        margin-bottom: 0px;
        -webkit-margin-start: 0px;
        margin-inline-start: 0px;
    }
</style>
<style data-emotion="css 7wbxco" data-s="">
    .css-7wbxco {
        text-align: center;
        font-size: 24px;
        font-weight: var(--chakra-fontWeights-bold);
    }
</style>
<style data-emotion="css ual471" data-s="">
    .css-ual471 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        padding-top: var(--chakra-space-6);
        padding-bottom: var(--chakra-space-6);
    }

    .css-ual471 > *:not(style) ~ *:not(style) {
        margin-top: var(--chakra-space-4);
        -webkit-margin-end: 0px;
        margin-inline-end: 0px;
        margin-bottom: 0px;
        -webkit-margin-start: 0px;
        margin-inline-start: 0px;
    }
</style>
<style data-emotion="css 1i4r6rq" data-s="">
    .css-1i4r6rq {
        display: grid;
        grid-gap: var(--chakra-space-2);
        grid-template-columns: 1fr 2fr;
        max-width: 900px;
        margin: 0 auto;
        text-align: justify;
        -webkit-padding-start: var(--chakra-space-6);
        padding-inline-start: var(--chakra-space-6);
        -webkit-padding-end: var(--chakra-space-6);
        padding-inline-end: var(--chakra-space-6);
    }
</style>
<style data-emotion="css 1d5j8vn" data-s="">
    .css-1d5j8vn {
        display: grid;
        grid-gap: var(--chakra-space-2);
        grid-template-rows: 1fr 1fr;
    }
</style>
<style data-emotion="css wtpnzt" data-s="">
    .css-wtpnzt {
        border-radius: 4px;
    }
</style>
<style data-emotion="css witmfg" data-s="">
    .css-witmfg {
        height: 100%;
        object-fit: cover;
        border-radius: 4px;
    }
</style>
<style data-emotion="css 17jzmug" data-s="">
    .css-17jzmug {
        font-size: 14px;
        text-align: center;
        font-style: italic;
    }
</style>
<style data-emotion="css caj8j" data-s="">
    .css-caj8j {
        padding-top: var(--chakra-space-8);
    }
</style>
<style data-emotion="css bfxxpj" data-s="">
    .css-bfxxpj {
        background: #fff;
        padding-top: var(--chakra-space-8);
        padding-bottom: var(--chakra-space-8);
    }
</style>
<style data-emotion="css 7b7t20" data-s="">
    .css-7b7t20 {
        color: #000;
    }
</style>
<style data-emotion="css 1dxoc93" data-s="">
    .css-1dxoc93 {
        width: 100%;
        object-fit: cover;
        border-radius: 4px;
    }
</style>
<style data-emotion="css 1shzb5" data-s="">
    .css-1shzb5 {
        margin-bottom: var(--chakra-space-8);
        min-height: 100vh;
        position: relative;
        z-index: 1;
    }
</style>
<style data-emotion="css u7aauf" data-s="">
    .css-u7aauf {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
    }
</style>
<style data-emotion="css 1axqb5i" data-s="">
    .css-1axqb5i {
        height: 100vh;
        width: 100%;
        object-fit: cover;
    }
</style>
<style data-emotion="css gw9m6h" data-s="">
    .css-gw9m6h {
        position: relative;
        z-index: 2;
        padding-top: 40vh;
        padding-bottom: 40vh;
    }
</style>
<style data-emotion="css 1w6641q" data-s="">
    .css-1w6641q {
        max-width: 1200px;
        margin: 0 auto;
        text-align: justify;
        -webkit-padding-start: var(--chakra-space-8);
        padding-inline-start: var(--chakra-space-8);
        -webkit-padding-end: var(--chakra-space-8);
        padding-inline-end: var(--chakra-space-8);
        background: rgba(221, 221, 221, 0.8);
        color: #000;
        padding-top: var(--chakra-space-24);
        padding-bottom: var(--chakra-space-24);
        border-radius: 4px;
    }
</style>
<style data-emotion="css evdf7q" data-s="">
    .css-evdf7q {
        max-width: 900px;
        margin: 0 auto;
    }
</style>
<style data-emotion="css 1vpdv9u" data-s="">
    .css-1vpdv9u {
        font-size: 20px;
        font-weight: var(--chakra-fontWeights-bold);
    }
</style>
<style data-emotion="css 1nefs71" data-s="">
    .css-1nefs71 {
        position: relative;
        z-index: 2;
        padding-top: 80vh;
        padding-bottom: 80vh;
    }
</style>
<style data-emotion="css enhs0q" data-s="">
    .css-enhs0q {
        margin-bottom: var(--chakra-space-4);
        margin-top: var(--chakra-space-8);
        min-height: 100vh;
        position: relative;
        z-index: 1;
    }
</style>
<style data-emotion="css 1mpyuzf" data-s="">
    .css-1mpyuzf {
        max-width: 900px;
        margin: 0 auto;
        margin-top: var(--chakra-space-4);
    }
</style>
<style data-emotion="css limngl" data-s="">
    .css-limngl {
        padding-top: var(--chakra-space-6);
        padding-bottom: var(--chakra-space-6);
    }
</style>
<style data-emotion="css wayys6" data-s="">
    .css-wayys6 {
        width: 100%;
        border-radius: 4px;
    }
</style>
<style data-emotion="css g621qz" data-s="">
    .css-g621qz {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        max-width: 900px;
        margin: 0 auto;
        margin-top: var(--chakra-space-4);
        gap: var(--chakra-space-2);
    }

    .css-g621qz > *:not(style) ~ *:not(style) {
        margin-top: 0.5rem;
        -webkit-margin-end: 0px;
        margin-inline-end: 0px;
        margin-bottom: 0px;
        -webkit-margin-start: 0px;
        margin-inline-start: 0px;
    }
</style>
<style data-emotion="css 14bs28v" data-s="">
    .css-14bs28v {
        margin-top: var(--chakra-space-6);
    }
</style>
<style data-emotion="css bbz8zn" data-s="">
    .css-bbz8zn {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        max-width: 900px;
        margin: 0 auto;
        margin-top: var(--chakra-space-4);
        gap: var(--chakra-space-4);
    }

    .css-bbz8zn > *:not(style) ~ *:not(style) {
        margin-top: 0.5rem;
        -webkit-margin-end: 0px;
        margin-inline-end: 0px;
        margin-bottom: 0px;
        -webkit-margin-start: 0px;
        margin-inline-start: 0px;
    }
</style>
<style data-emotion="css u9bk53" data-s="">
    .css-u9bk53 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        max-width: 900px;
        margin: 0 auto;
        gap: var(--chakra-space-4);
    }

    .css-u9bk53 > *:not(style) ~ *:not(style) {
        margin-top: 0.5rem;
        -webkit-margin-end: 0px;
        margin-inline-end: 0px;
        margin-bottom: 0px;
        -webkit-margin-start: 0px;
        margin-inline-start: 0px;
    }
</style>
<style data-emotion="css 1ddtiue" data-s="">
    .css-1ddtiue {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        max-width: 900px;
        position: relative;
        z-index: 1;
        margin: 0 auto;
        text-align: justify;
        -webkit-padding-start: var(--chakra-space-6);
        padding-inline-start: var(--chakra-space-6);
        -webkit-padding-end: var(--chakra-space-6);
        padding-inline-end: var(--chakra-space-6);
        overflow-x: hidden;
    }

    .css-1ddtiue > *:not(style) ~ *:not(style) {
        margin-top: var(--chakra-space-4);
        -webkit-margin-end: 0px;
        margin-inline-end: 0px;
        margin-bottom: 0px;
        -webkit-margin-start: 0px;
        margin-inline-start: 0px;
    }
</style>
<style data-emotion="css 3k6px3" data-s="">
    .css-3k6px3 {
        text-align: right;
        font-style: italic;
    }
</style>
<style data-emotion="css 1sa3z1a" data-s="">
    .css-1sa3z1a {
        font-weight: var(--chakra-fontWeights-bold);
        text-align: right;
    }
</style>
<style data-emotion="css v6hpyw" data-s="">
    .css-v6hpyw {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        max-width: 900px;
        position: relative;
        z-index: 1;
        margin: 16px auto 0 auto;
        text-align: justify;
        -webkit-padding-start: var(--chakra-space-6);
        padding-inline-start: var(--chakra-space-6);
        -webkit-padding-end: var(--chakra-space-6);
        padding-inline-end: var(--chakra-space-6);
    }

    .css-v6hpyw > *:not(style) ~ *:not(style) {
        margin-top: var(--chakra-space-4);
        -webkit-margin-end: 0px;
        margin-inline-end: 0px;
        margin-bottom: 0px;
        -webkit-margin-start: 0px;
        margin-inline-start: 0px;
    }
</style>
<style data-emotion="css d7nvhw" data-s="">
    .css-d7nvhw {
        font-weight: var(--chakra-fontWeights-bold);
        font-size: var(--chakra-fontSizes-3xl);
        line-height: 1.33;
        font-family: Sansita Swashed;
    }

    @media screen and (min-width: 48em) {
        .css-d7nvhw {
            font-size: var(--chakra-fontSizes-4xl);
            line-height: 1.2;
        }
    }
</style>
<style data-emotion="css 1btehal" data-s="">
    .css-1btehal {
        display: grid;
        grid-gap: var(--chakra-space-6);
        grid-template-columns: 1fr;
    }

    @media screen and (min-width: 30em) {
        .css-1btehal {
            grid-template-columns: repeat(3, 1fr);
        }
    }
</style>
<style data-emotion="css won06e" data-s="">
    .css-won06e {
        width: 100%;
        position: relative;
        cursor: pointer;
        border-radius: 4px;
        overflow: hidden;
        -webkit-transition: all 0.3s;
        transition: all 0.3s;
        box-shadow: #000 0px 8px 30px;
    }

    .css-won06e:hover,
    .css-won06e[data-hover] {
        box-shadow: #000 0px 8px 40px;
        -webkit-transform: scale(1.04);
        -moz-transform: scale(1.04);
        -ms-transform: scale(1.04);
        transform: scale(1.04);
    }
</style>
<style data-emotion="css 6uxxj6" data-s="">
    .css-6uxxj6 {
        width: 100%;
        border-radius: 4px;
        height: 200px;
        object-fit: cover;
    }
</style>
<style data-emotion="css 4eunz1" data-s="">
    .css-4eunz1 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: end;
        -ms-flex-pack: end;
        -webkit-justify-content: flex-end;
        justify-content: flex-end;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        position: absolute;
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
        padding: var(--chakra-space-3);
        background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.9) 70%);
    }

    .css-4eunz1 > *:not(style) ~ *:not(style) {
        margin-top: 0px;
        -webkit-margin-end: 0px;
        margin-inline-end: 0px;
        margin-bottom: 0px;
        -webkit-margin-start: 0px;
        margin-inline-start: 0px;
    }
</style>
<style data-emotion="css 1jkqms3" data-s="">
    .css-1jkqms3 {
        font-family: Signika;
        font-size: 18px;
        line-height: 1.2;
    }
</style>
