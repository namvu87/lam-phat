<html>

<head>
    <meta charset="utf-8">
    <title>Nhóm 6: Việt Nam hướng đến xuất nhập khẩu bền vững</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="Nhóm 6: Việt Nam hướng đến xuất nhập khẩu bền vững"/>
    <meta property="og:image:alt" content="Nhóm 6: Việt Nam hướng đến xuất nhập khẩu bền vững">
    <meta property="og:image" content="{!! asset('favicon.ico') !!}">
    <link rel="icon" href="{!! asset('favicon.ico') !!}">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lipis/flag-icons@7.0.0/css/flag-icons.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous"/>
    <link
        href="https://fonts.googleapis.com/css2?family=Bellota&family=Oooh+Baby&family=Patrick+Hand&family=Phudu&family=WindSong:wght@500&family=Arizonia&family=Mynerve&family=Nunito&family=Big+Shoulders+Stencil+Text&family=Signika&family=Srisakdi&family=Signika&family=Allura&family=Sansita+Swashed&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="statics/css/style.css">
    <script src="statics/js/jquery.min.js" type="text/javascript"></script>
    <script src="statics/js/swiper.js" type="text/javascript"></script>
    <script src="statics/js/aos.js" type="text/javascript"></script>
    <script src="statics/js/js_custom.js" type="text/javascript"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="https://cdn.canvasjs.com/jquery.canvasjs.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
    <link rel="stylesheet" href="statics/snippets-custom.bundle.min.css">

    <style>
        .custom-select .option.active {
            background-color: #f0f0f0;
            /* Màu nền tùy chọn được chọn */
            color: #333;
            /* Màu chữ của tùy chọn được chọn */
        }

        /* CSS khi rê chuột qua tùy chọn */
        .custom-select .option:hover {
            background-color: #eaeaea;
            /* Màu nền khi rê chuột qua tùy chọn */
            cursor: pointer;
        }

        .custom-select-country .option.active {
            background-color: #f0f0f0;
            /* Màu nền tùy chọn được chọn */
            color: #333;
            /* Màu chữ của tùy chọn được chọn */
        }

        /* CSS khi rê chuột qua tùy chọn */
        .custom-select-country .option:hover {
            background-color: #eaeaea;
            /* Màu nền khi rê chuột qua tùy chọn */
            cursor: pointer;
        }

        .chart-container-pie {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .pie-chart {
            flex: 1 0 33%;
            margin-left: 1%;

        }

        @media screen and (max-width: 768px) {
            .chart-container-pie {
                flex-direction: column; /* Hiển thị các biểu đồ theo chiều dọc */
            }

            .pie-chart {
                margin-bottom: 20px; /* Khoảng cách giữa các biểu đồ */
            }

            /* Điều chỉnh kích thước của biểu đồ */
            .pie-chart > div {
                width: 100%;
                max-width: 250px;
                height: 200px;
                margin: 0 auto;
            }
        }
    </style>
</head>

<body class="chakra-ui-dark">

<div id="wapper">
    <div class="css-1q02ksg">
        <style>
            .css-1yiqmvn {
                background-image: url('1.png');
                background-size: 100% 100%;
                background-attachment: fixed;
                background-position: right;
            }


        </style>
        <div style="opacity:0">

            @if(\Agent::isMobile())
                <div class="chakra-stack css-3ir41n">
                    <div data-aos="zoom-in">
                        <img class="chakra-image css-wayys6" src="1.png"/>
                    </div>

                </div>
            @else
                <div data-aos-offset="500" data-aos-duration="500"
                     style="background-image:url(1.png);background-size:cover;background-position: right"
                     class="css-1yiqmvn">


                </div>
            @endif

            <div data-aos="fade-up" class="chakra-ui-transform">
                <div class="css-k1dh30">
                    <div data-aos="zoom-up"  style="margin-top: 100px;">
                        <p class="chakra-text css-umoeaw css-umoeaw-update " style="text-align: justify">
                            <b>Xác định xuất nhập khẩu đóng vai trò quan trọng trong nền kinh tế, Bộ Công Thương đã
                                xây dựng và trình Thủ tướng Chính phủ phê duyệt, ban hành Quyết định số 493/QĐ-TTg
                                ngày 19/4/2022 phê duyệt Chiến lược xuất nhập khẩu hàng hóa đến năm 2030. Chiến lược
                                có định hướng phát triển xuất nhập khẩu bền vững với cơ cấu cân đối, hài hoà về cán
                                cân thương mại và nâng cao vị thế quốc gia trong chuỗi giá trị toàn cầu.
                            </b>
                        <p class="chakra-text" style="text-align: justify">
                            Việc thực hiện Chiến lược xuất nhập khẩu hàng hóa được thực hiện trên ba quan điểm cơ
                            bản xuyên suốt. Cụ thể, quan điểm Chiến lược nhấn mạnh yêu cầu phát triển xuất khẩu bền
                            vững, đồng thời chỉ rõ các yếu tố để đạt được sự bền vững: hài hòa về cơ cấu, cán cân
                            thương mại, về mục tiêu ngắn hạn và dài hạn, về công bằng xã hội, về bảo vệ môi trường.
                        </p>
                        <p class="chakra-text" style="text-align: justify">

                            Chiến lược cũng đề cập vấn đề thương mại xanh, thương mại công bằng đang là mối quan tâm
                            của các nước phát triển trên thế giới. Đồng thời bày tỏ rõ quan điểm về phát triển xuất
                            nhập khẩu gắn với các động lực mới: kinh tế số, kinh tế xanh, kinh tế tuần hoàn và đổi
                            mới sáng tạo. Thực hiện xuất nhập khẩu gắn với quy hoạch, kế hoạch phát triển địa phương
                            nhằm phát huy lợi thế cạnh tranh. Từ đó, tham gia sâu vào chuỗi cung ứng và chuỗi giá
                            trị toàn cầu.

                        </p>
                    </div>
                    <div data-aos="fade-up" data-aos-offset="200">
                        <h2 class="chakra-heading css-kjiv5n font-style-data">
                            Việt Nam bắt đầu nổi lên <br> trên bản đồ xuất nhập khẩu toàn cầu
                        </h2>
                    </div>
                    <div data-aos="zoom-up"  style="margin-top: 12px;">
                        <p class="chakra-text" style="text-align: justify">

                            Việt Nam bắt đầu thực hiện công cuộc Đổi mới vào giữa tháng 12/1986. Một năm sau, Luật
                            Đầu tư nước ngoài tại Việt Nam ra đời. Bởi thế, hành trình 30 năm thu hút đầu tư trực
                            tiếp nước ngoài (FDI) của Việt Nam cũng gần như song trùng với hơn 3 thập kỷ Đổi mới của
                            nền kinh tế. Và dù vẫn còn những phân vân giữa được và mất, song mở cửa thu hút FDI
                            chính là một trong những quyết định sáng suốt nhất để Việt Nam từ một nước nghèo, lạc
                            hậu trở thành một trong những điểm sáng của kinh tế toàn cầu.
                        </p>
                        <p class="chakra-text" style="text-align: justify; margin-top:5px;">
                            Nhưng quan trọng hơn cả, nhờ có tri thức, công nghệ của nhà đầu tư nước ngoài, đã có
                            những con đường vượt đầm lầy được xây dựng; nhiều ngành kinh tế hình thành, phát triển
                            và Việt Nam - một nước nông nghiệp lạc hậu - đã chuyển mình, ghi dấu ấn trên bản đồ công
                            nghệ thế giới, có thể kể tới một số dấu mốc quan trọng trong khởi đầu của Việt Nam.
                        </p>
                        <p class="chakra-text" style="text-align: justify; margin-top:5px;">

                            Năm 1993 là năm đánh dấu bước phát triển mới trong hoạt động xuất khẩu của Việt Nam qua
                            việc Bộ thương mại ban hành Quyết định số 405-TM/XNK ngày 13/04/1993 công bố các danh
                            mục hàng hoá xuất khẩu, nhập khẩu qua các cửa khẩu Việt Nam.
                        </p>
                        <p class="chakra-text" style="text-align: justify; margin-top:5px;">

                            Đến năm 2012 là lần đầu tiên Việt Nam ghi nhận xuất siêu 284 triệu USD. Đây là “kỳ tích”
                            sau 20 năm Việt Nam triền miên nhập siêu. Động lực xuất khẩu của Việt Nam được thúc đẩy
                            khi tham gia vào 16 Hiệp định thương mại tự do.
                        </p>
                    </div>
                </div>

                @if(\Agent::isMobile())
                    <div class="chakra-stack css-3ir41n">
                        <div data-aos="zoom-in">
                            <img class="chakra-image css-wayys6" src="bd1.png"/>
                        </div>

                    </div>
                @else
                    <div data-aos-offset="500" data-aos-duration="500"
                         style="background-image:url(bd1.png);background-size:cover;background-position: right"
                         class="css-1yiqmvn">


                    </div>
                @endif

            </div>

            <div class="chakra-stack css-1o07fwd" style="margin-top: 3px">

                <div data-aos="zoom-up"  style="margin-top: 10px">
                    <p >
                        Trong giai đoạn từ năm 2012 đến năm 2023, cán cân thương mại của Việt Nam đã trải qua
                        nhiều biến động đáng chú ý, phản ánh sự điều chỉnh và phát triển của nền kinh tế quốc
                        gia. Ban đầu, trong giai đoạn đầu của thập kỷ, Việt Nam gặp phải những thách thức lớn
                        khi cán cân thương mại có xu hướng thâm hậu do sự nhập khẩu tăng nhanh và thiếu hụt
                        trong ngành xuất khẩu.
                    </p>
                    <p >


                        Tuy nhiên, qua những nỗ lực đáng kể trong việc đổi mới và đa dạng hóa cấu trúc sản xuất,
                        Việt Nam đã từng bước cải thiện cán cân thương mại. Đặc biệt, những năm gần đây, sự chú
                        trọng vào các ngành công nghiệp có giá trị gia tăng cao đã đóng góp tích cực vào cán cân
                        thương mại tích lũy dư dụng. Xuất khẩu các sản phẩm có giá trị cao như điện tử, ô tô, và
                        dịch vụ đã đóng vai trò quan trọng trong việc cải thiện tình hình thương mại của Việt
                        Nam.
                    </p>
                    <p >

                        Cùng với đó, việc đàm phán và ký kết các hiệp định thương mại quốc tế, như Hiệp định
                        Thương mại EU - Việt Nam và Hiệp định đối tác toàn diện và tiến bộ xuyên Thái Bình Dương
                        (CPTPP), đã mở ra cơ hội mới, giúp mở rộng thị trường xuất khẩu và giảm thiểu rủi ro
                        thương mại. Tính đến nay, cán cân thương mại của Việt Nam đã thể hiện sự ổn định và tích
                        cực, phản ánh khả năng cạnh tranh và thích ứng của nền kinh tế Việt Nam trước những
                        thách thức toàn cầu.


                    </p>
                </div>

                {{-- biểu đồ 1 --}}
                <div class="chakra-stack css-1o07fwd" style="margin-top: 3px">
                    <div data-aos="zoom-up"  style="margin-top: 30px;">
                        <div id="nhapsieu-container-data" style="width: 100%"></div>

                        <div data-aos="zoom-up" style="margin-top: 9px;">
                            <p class="chakra-text css-17jzmug" style="text-align: center"> Xuất siêu, nhập siêu qua các năm (đơn vị: tỷ USD)</p>
                        </div>
                    </div>
                </div>

                <div data-aos="zoom-up" >
                    <p >
                        Trong giai đoạn từ 2018 đến 2023, Tổng kim ngạch xuất nhập khẩu của Việt Nam phải đối
                        mặt với nhiều thách thức như thị trường biến động, tình trạng chiến tranh thương mại
                        toàn cầu và ảnh hưởng của đại dịch Covid-19. Trong bối cảnh đó, Việt Nam đã đưa ra những
                        nỗ lực nhất định để giảm thiểu độ nghịch lý thương mại thông qua đối thoại và đàm phán.
                        Điều này không chỉ giúp duy trì ổn định quan hệ thương mại mà còn tạo ra môi trường
                        thuận lợi cho sự phát triển bền vững.
                    </p>
                    <p >


                        Có thể nói, giai đoạn này là một chặng đường đầy thách thức tuy nhiên cũng là cơ hội cho
                        xuất nhập khẩu Việt Nam khi đã thể hiện sự đổi mới và đa dạng hóa trong chiến lược
                        thương mại. Ngoài ra, sự đa dạng hoá thị trường, tăng trưởng ổn định và sự hỗ trợ từ FDI
                        cũng là một trong những yếu tố quan trọng giúp Việt Nam tiến bộ hơn trong lĩnh vực xuất
                        nhập khẩu.


                    </p>
                </div>
                {{-- biểu đồ 2 --}}
                <div class="chakra-stack css-1o07fwd" style="margin-top: 3px">
                    <div data-aos="zoom-up"  style="margin-top: 30px;">
                        <div id="chart-container-data" style="width: 100%"></div>

                        <div data-aos="zoom-up" style="margin-top: 9px;">
                            <p class="chakra-text css-17jzmug" style="text-align: center;">
                                Tổng kim ngạch xuất nhập khẩu 2018 - 2023 (đơn vị: tỷ USD)</p>
                        </div>
                    </div>
                </div>

                {{-- <div class="chakra-stack css-3ir41n">
                        <div id="chart-container" style="min-width: 850px; height: 400px; margin: 0 auto"></div>

                        <div data-aos="zoom-up" style="margin-top: 9px;">
                            <p class="chakra-text css-17jzmug" style="text-align: center"> Xuất siêu, nhập siêu qua
                                các năm
                                (đơn vị: tỷ USD)</p>
                        </div>
                    </div> --}}


                <div data-aos="zoom-up" >
                    <p >
                        Với những bước tiến trong việc xuất nhập khẩu, năm 2021, Tổ chức Thương mại Thế giới
                        (WTO) ghi nhận xuất khẩu của Việt Nam xếp hạng thứ 23 trên thế giới và nhập khẩu của
                        Việt Nam xếp hạng thứ 20 trên thế giới. Trong ASEAN, xếp hạng xuất nhập khẩu hàng hóa
                        của Việt Nam đều đứng ở vị trí thứ 2 (chỉ sau Singapore).
                    </p>
                    <p >

                        Lĩnh vực xuất khẩu của Việt Nam đang ngày càng phát triển mạnh với danh mục hàng hóa đa
                        dạng và đã gặt hái được nhiều thành tích ấn tượng. Trong đó có 10 loại hàng xuất khẩu
                        chủ lực của doanh nghiệp trong nước phải kể đến là: Dệt may, Gỗ và sản phẩm gỗ, Cao su,
                        Cà phê, Sắt thép các loại, Thuỷ sản, Gạo, Rau quả, Sản phẩm từ chất dẻo, Sản phẩm từ sắt
                        thép.

                    </p>
                </div>

                {{-- biểu đồ 3 --}}

                <div class="container-fluid">
                    <div class="row">


                        @if(\Agent::isMobile())
                            <div class="col-md-3">
                                <div class="custom-select" id="country-selector">
                                <span class="option" data-value="detmay">
                                    <i class="fas fa-industry"></i> Dệt may</span>
                                    <span class="option" data-value="go"><i
                                            class="fas fa-tree"></i> Gỗ và sản phẩm gỗ</span>
                                    <span class="option" data-value="caosu"><i class="fas fa-tint"></i> Cao su</span>
                                    <span class="option" data-value="coffee"><i class="fas fa-coffee"></i> Cà phê</span>
                                    <span class="option" data-value="fe"><i class="fas fa-industry"></i> Sắt thép</span>
                                    <span class="option" data-value="fish"><i class="fas fa-fish"></i> Thủy sản</span>
                                    <span class="option" data-value="rice"><i class="fas fa-utensils"></i> Gạo</span>
                                    <span class="option" data-value="rau"><i class="fas fa-carrot"></i> Rau quả</span>
                                    <span class="option" data-value="deo"><i class="fas fa-recycle"></i> Sản phẩm từ chất
                                dẻo</span>
                                    <span class="option" data-value="productFe"><i class="fas fa-industry"></i> Sản phẩm từ
                                sắt
                                thép</span>
                                    <br>
                                    <span class="icon-text">
                                <img src="kindpng_1523209.png" alt="" width="20px" height="10px">
                                <i>Click vào biểu tượng để xem tiếp biểu đồ</i>

                            </span>
                                    <!-- Thêm các thẻ span cho các tùy chọn khác -->
                                </div>
                            </div>

                        @else
                            <div class="col-md-3">
                                <div class="custom-select" id="country-selector">
                                    <div class="row">
                                        <div class="col-md-6 mb-3" style="margin-top: 15px;">
                                            <span class="option" data-value="detmay">
                                                <img class="img-fluid" src="detmay.png" alt="Dệt may" title="Dệt may"
                                                     style="height: 40px;">
                                                </span>

                                        </div>
                                        <div class="col-md-6 mb-3" style="margin-top: 15px;">
                                             <span class="option" data-value="go">
                                                <img class="img-fluid" src="go-sp-go.png" title="Gỗ và sản phẩm gỗ"
                                                     style="height: 40px;"
                                                     alt="">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mb-3" style="margin-top: 15px;">
                                            <span class="option" data-value="caosu">
                                                 <img class="img-fluid" src="caosu.png" title="Cao su" alt=""
                                                      style="height: 40px;">
                                                </span>


                                        </div>
                                        <div class="col-md-6 mb-3" style="margin-top: 15px;">

                                         <span class="option" data-value="coffee">
                                                 <img class="img-fluid" src="caphe.png" alt="" title="Cà phê"
                                                      style="height: 40px;">
                                                </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mb-3" style="margin-top: 15px;">
                                            <span class="option" data-value="fe">
                                                 <img class="img-fluid" src="satthep.png" alt=""
                                                      title="Sắt thép các loại"
                                                      style="height: 40px;">
                                                </span>

                                        </div>

                                        <div class="col-md-6 mb-3" style="margin-top: 15px;">
                                                 <span class="option" data-value="fish">
                                                <img class="img-fluid" src="thuysan.png" alt="" title="Thủy sản"
                                                     style="height: 40px;"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 mb-3" style="margin-top: 15px;">
                                            <span class="option" data-value="rice">
                                                 <img class="img-fluid" src="gao.png" alt="" title="Gạo"
                                                      style="height: 40px;"></span>

                                        </div>

                                        <div class="col-md-6 mb-3" style="margin-top: 15px;">
                                                <span class="option" data-value="rau">
                                                 <img class="img-fluid" src="rauqua.png" alt="" title="Rau quả"
                                                      style="height: 40px;">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mb-3" style="margin-top: 15px;">

                                            <span class="option" data-value="deo">
                                              <img class="img-fluid" src="deo.png" alt="" title="Sản phẩm từ chất dẻo"
                                                   style="height: 40px;">
                                             </span>
                                        </div>

                                        <div class="col-md-6 mb-3" style="margin-top: 15px;">

                                        <span class="option" data-value="productFe">
                                               <img class="img-fluid" src="spsatthep.png" title="Sản phẩm từ sắt thép"
                                                    alt=""
                                                    style="height: 40px;">
                                              </span>
                                        </div>
                                    </div>


                                    <br>
                                    <span class="icon-text">
                                <img src="kindpng_1523209.png" alt="" width="20px" height="10px">
                                <i>Click vào biểu tượng để xem tiếp biểu đồ</i>

                            </span>
                                    <!-- Thêm các thẻ span cho các tùy chọn khác -->
                                </div>
                            </div>
                        @endif
                        <div class="col-md-9">
                            <div id="detmay-container"
                                 style="min-width: 100%; height: 400px; margin: 0 auto;"></div>
                            <div id="go-container"
                                 style="min-width: 310px; height: 400px; margin: 0 auto;display: none;">
                            </div>
                            <div id="caosu-container"
                                 style="min-width: 310px; height: 400px; margin: 0 auto;display: none;">
                            </div>
                            <div id="coffee-container"
                                 style="min-width: 310px; height: 400px; margin: 0px auto;display: none;">
                            </div>
                            <div id="fe-container"
                                 style="min-width: 310px; height: 400px; margin: 0px auto;display: none;">
                            </div>
                            <div id="fish-container"
                                 style="min-width: 310px; height: 400px; margin: 0 auto;display: none;"></div>
                            <div id="rice-container"
                                 style="min-width: 310px; height: 400px; margin: 0 auto;display: none;"></div>
                            <div id="rau-container"
                                 style="min-width: 310px; height: 400px; margin: 0px auto;display: none;">
                            </div>
                            <div id="deo-container"
                                 style="min-width: 310px; height: 400px; margin: 0px auto;display: none;">
                            </div>
                            <div id="productFe-container"
                                 style="min-width: 310px; height: 400px; margin: 0px auto;display: none;"></div>
                            <div data-aos="zoom-up" style="margin-top: 9px;">
                                <p class="chakra-text css-17jzmug" style="text-align: center"> 10 mặt hàng xuất khẩu
                                    chủ lực của doanh nghiệp trong nước (đơn vị: tỷ USD)</p>
                            </div>
                        </div>

                    </div>


                    <div class="chakra-stack css-3ir41n">
                        {{-- <div id="chart-container" style="min-width: 850px; height: 400px; margin: 0 auto"></div> --}}


                    </div>


                    <div data-aos="zoom-up" >
                        <p >
                            Bên cạnh sự phát triển đáng kể trong trong chiến lược thương mại thì có một sự thật cần
                            nhìn nhận là Việt Nam đang phụ thuộc vào một số thị trường xuất khẩu lớn như Mỹ, Trung
                            Quốc hay Nhật Bản. Theo Tổng cục thống kê, thị trường xuất khẩu hàng hóa 6 tháng đầu năm
                            2023, nhìn chung, các ngành hàng đều gặp khó khăn về thị trường xuất khẩu do tổng cầu
                            trên thế giới giảm, nhất là là đối với hàng hoá tiêu dùng không thiết yếu, do vậy kim
                            ngạch xuất khẩu trong 6 tháng đầu năm 2023 của nước ta sang hầu hết các thị trường chủ
                            lực đều giảm, tổng kim ngạch xuất khẩu hàng hóa ước đạt 164,68 tỷ USD, giảm 12,3% so với
                            cùng kỳ năm trước.
                        </p>

                        <p >

                            Về các thị trường cụ thể, Hoa Kỳ vẫn là thị trường lớn nhất của Việt Nam trong 6 tháng
                            đầu năm 2023 khi chiếm gần 27% tổng kim ngạch xuất khẩu cả nước. Đối với thị trường xuất
                            khẩu lớn thứ hai của Việt Nam - Trung Quốc, mặc dù thị trường này đã mở cửa biên giới từ
                            ngày 08/01/2023 song kinh tế Trung Quốc chưa phục hồi như kỳ vọng. Đồng thời các tiêu
                            chuẩn đối với hàng hoá nhập khẩu của Trung Quốc đặt ra ngày càng khắt khe. Đây là những
                            nguyên nhân chủ yếu dẫn đến kim ngạch xuất khẩu của Việt Nam sang Trung Quốc trong 6
                            tháng đầu năm 2023 ước đạt 25.92 tỷ USD, giảm 9,3% so với cùng kỳ.


                        </p>
                    </div>
                    {{-- //biểu đồ 4 --}}
                    <div>

                        <div id="map"></div>
                        <div class="custom-select-country" id="country-selector4" style="margin-bottom: 40px;">

                        <span class="icon-text">
                                <img src="kindpng_1523209.png" alt="" width="20px" height="10px">
                                <i>Click vào biểu tượng để xem tiếp biểu đồ</i>

                            </span>
                        </div>
                        <div class="chart-columns">
                            <div class="chart-column">
                                <div id="VN-container-data-chart" style="min-width: 310px; height: 400px;"></div>
                                <div class="chakra-stack css-3ir41n">
                                    {{-- <div id="chart-container" style="min-width: 850px; height: 400px; margin: 0 auto"></div> --}}

                                    <div data-aos="zoom-up" style="margin-top: -12px;">
                                        <p class="chakra-text css-17jzmug"
                                           style="text-align: center"> Tổng kim ngạch xuất khẩu của Việt Nam (đơn vị: tỷ
                                            USD)
                                        </p>
                                    </div>
                                </div>

                            </div>

                            <div class="chart-column">
                                <div id="GB-container-data"
                                     style="min-width: 310px; height: 400px; margin: 0 auto;">
                                </div>
                                <div id="JP-container-data"
                                     style="min-width: 310px; height: 400px; margin: 0 auto;display: none;"></div>
                                <div id="HK-container-data"
                                     style="min-width: 310px; height: 400px; margin: 0 auto;display: none;"></div>
                                <div id="KR-container-data"
                                     style="min-width: 310px; height: 400px; margin: 0px auto;display: none;"></div>
                                <div id="CN-container-data"
                                     style="min-width: 310px; height: 400px; margin: 0px auto;display: none;"></div>
                                <div id="TH-container-data"
                                     style="min-width: 310px; height: 400px; margin: 0px auto;display: none;"></div>
                                <div id="IN-container-data"
                                     style="min-width: 310px; height: 400px; margin: 0px auto;display: none;"></div>
                                <div id="DE-container-data"
                                     style="min-width: 310px; height: 400px; margin: 0px auto;display: none;"></div>
                                <div id="NL-container-data"
                                     style="min-width: 310px; height: 400px; margin: 0px auto;display: none;"></div>
                                <div id="US-container-data"
                                     style="min-width: 310px; height: 400px; margin: 0px auto;display: none;"></div>
                                <div class="chakra-stack css-3ir41n">
                                    {{-- <div id="chart-container" style="min-width: 850px; height: 400px; margin: 0 auto"></div> --}}

                                    <div data-aos="zoom-up" style="margin-top: -12px;">
                                        <p class="chakra-text css-17jzmug" id="change_name"
                                           style="text-align: center"> Kim ngạch xuất khẩu sang
                                            Anh
                                        </p>
                                    </div>
                                </div>
                                <!-- Thêm các div của các biểu đồ các nước khác -->
                            </div>

                        </div>

                        <style>
                            .chart-columns {
                                display: flex;
                            }

                            .chart-column {
                                flex: 1;
                                margin-right: 10px; /* Khoảng cách giữa hai cột */
                            }

                            @media screen and (max-width: 768px) {
                                .chart-columns {
                                    flex-direction: column;
                                }

                                .chart-column {
                                    margin-bottom: -207px; /* Khoảng cách giữa các cột */
                                }

                                /* Điều chỉnh kích thước của các biểu đồ khi hiển thị theo chiều dọc */
                                .chart-column > div {
                                    min-width: 100%;
                                    height: 300px; /* Điều chỉnh chiều cao của biểu đồ */
                                    margin: 0 auto;
                                }
                            }

                        </style>
                    </div>
                    <div data-aos="zoom-up" >
                        <p >
                            Theo một số chuyên gia dự báo, triển vọng năm 2024 sẽ có nhiều điểm sáng tích cực nhưng
                            xuất khẩu vẫn tiềm ẩn rủi ro. Bởi Việt Nam có độ mở kinh tế cao nên cũng phụ thuộc lớn
                            vào xuất khẩu. Trường hợp các thị trường xuất khẩu chính là Mỹ, châu Âu, Trung Quốc hồi
                            phục kém hơn các dự báo đưa ra thời điểm này, họ sẽ giảm nhập khẩu.Vì vậy, khi các quốc
                            gia này rơi vào giai đoạn khủng hoảng, hoạt động xuất khẩu của Việt Nam đối mặt với
                            những cú sốc lớn và bị gián đoạn.
                        </p>

                        <p >

                            Mặt khác, các nhà sản xuất Việt Nam đang gặp nhiều thách thức khi sản phẩm chưa đáp ứng
                            yêu cầu chuyển đổi năng lượng xanh – sạch, sản xuất cacbon thấp, vật liệu bền vững, sản
                            xuất thân thiện môi trường… từ thị trường nhập khẩu
                        </p>
                        <p >

                            Chẳng hạn, với Liên minh châu Âu (EU), ngoài các FTA thế hệ mới như EVFTA (Hiệp định thương mại tự do Việt Nam -
                            EU)
                            hay CPTPP (Hiệp định Ðối tác Toàn diện và Tiến bộ Xuyên Thái Bình Dương), thị trường này
                            tiếp
                            tục đặt ra nhiều yêu cầu ngặt nghèo với hàng nhập khẩu. Từ hàng nông - thủy sản đến dệt may,
                            giày dép, EU bổ sung các quy định mới bảo vệ sức khỏe người tiêu dùng, hay yêu cầu về sản
                            xuất
                            xanh, bền vững đối với môi trường


                        </p>
                    </div>
                    <div data-aos="fade-up" data-aos-offset="200">
                        <h2 class="chakra-heading css-kjiv5n font-style-data">
                            Tình trạng mất cân đối <br> trong hoạt động xuất nhập khẩu
                        </h2>
                    </div>
                    <div data-aos="zoom-up" >
                        <p >
                            Mặc dù xuất khẩu của Việt Nam đang dần xác lập được vị thế cạnh tranh trên thị trường
                            toàn cầu. Tuy nhiên, Việt Nam lại chiếm lĩnh thị trường trên thế giới chủ yếu ở nhóm
                            hàng hóa cơ bản, như: dầu mỏ và khoáng sản, nông sản, hàng dệt may, da giày, thủy sản,
                            đồ gỗ và điện tử. Đây là những ngành thâm dụng lao động lớn, nhưng về xu thế không còn
                            tăng trưởng nhanh trên thế giới, đồng thời rất dễ bị ảnh hưởng bởi việc hạ thấp chi phí
                            từ các đối thủ mới, có chi phí lao động thấp.
                        </p>
                        <p >


                            Khá nhiều các mặt hàng xuất khẩu, kể cả những mặt hàng có kim ngạch lớn chưa có thương
                            hiệu riêng, xuất khẩu thường phải thông qua đối tác khác nên giá bán thường thấp hơn sản
                            phẩm cùng loại của các nước khác. Trong 6 tháng đầu năm 2023, kim ngạch xuất khẩu sụt
                            giảm mạnh 12% so với cùng kỳ tương đương giảm 21,3% tỷ USD, phần lớn do mức sụt giảm
                            xuất khẩu của khối FDI (15,5 tỷ USD).
                        </p>
                    </div>
                    {{-- biểu đồ 5 --}}

                    <div class="chakra-stack css-1o07fwd" style="margin-top: 3px">
                        <div data-aos="zoom-up"  style="margin-top: 30px;">
                            <div id="chart-container5" style="width:100%"></div>

                            <div data-aos="zoom-up" style="margin-top: 9px;">
                                <p class="chakra-text css-17jzmug" style="text-align: center">
                                    Kim ngạch xuất khẩu 6 tháng đầu các năm

                                </p>
                            </div>
                        </div>
                    </div>
                    <div data-aos="zoom-up" >
                        <p >
                            Xuất - nhập khẩu tăng trưởng với nhịp độ bình quân khá cao về kim ngạch, đa dạng và
                            phong phú về mặt hàng, thị trường xuất khẩu ngày càng được mở rộng tuy nhiên về lâu dài,
                            xuất khẩu phụ thuộc quá nhiều vào khu vực FDI tiềm ẩn rủi ro đối với nền kinh tế của
                            Việt Nam.
                        </p>
                    </div>
                    {{-- biểu đồ 6 --}}
                    <div class="chart-container-pie" style="margin-left: -250px">

                        {{-- <div class="pie-chart pie-2018">
                            <div id="pie-2018"></div>
                        </div>
                        <div class="pie-chart pie-2019">
                            <div id="pie-2019"></div>
                        </div> --}}


                    </div>


                    <div class="chart-container-pie">


                        <div class="pie-chart pie-2018">
                            <div id="pie-2018" style="width: 250px; height: 200px; margin: 0 auto">

                            </div>
                            <div data-aos="zoom-up" style="margin-top: 9px;">
                                <p class="chakra-text css-17jzmug" style="text-align: center"> 2018

                                </p>
                            </div>
                        </div>
                        <style>
                            @media screen and (max-width: 768px) {
                                .pie-2018 {
                                    margin-bottom: -138px;
                                }
                            }
                        </style>
                        <div class="pie-chart pie-2019">
                            <div id="pie-2019" style="width: 250px; height: 200px; margin: 0 auto">

                            </div>
                            <p class="chakra-text css-17jzmug" style="text-align: center"> 2019

                            </p>
                        </div>

                        <div class="pie-chart pie-2020">
                            <div id="pie-2020" style="width: 250px; height: 200px; margin: 0 auto">

                            </div>
                            <p class="chakra-text css-17jzmug" style="text-align: center"> 2020

                            </p>
                        </div>
                    </div>
                    <div class="chart-container-pie">


                        <div class="pie-chart pie-2021">
                            <div id="pie-2021" style="width: 250px; height: 200px; margin: 0 auto">

                            </div>
                            <p class="chakra-text css-17jzmug" style="text-align: center"> 2021

                            </p>
                        </div>
                        <div class="pie-chart pie-2022">
                            <div id="pie-2022" style="width: 250px; height: 200px; margin: 0 auto">

                            </div>
                            <p class="chakra-text css-17jzmug" style="text-align: center"> 2022

                            </p>
                        </div>

                        <div class="pie-chart pie-2023">
                            <div id="pie-2023" style="width: 250px; height: 200px; margin: 0 auto">

                            </div>
                            <p class="chakra-text css-17jzmug" style="text-align: center"> 2023 (6 tháng)

                            </p>
                        </div>
                    </div>

                    <div class="chakra-stack css-3ir41n">


                        {{-- <div id="chart-container" style="min-width: 850px; height: 400px; margin: 0 auto"></div> --}}

                        <div data-aos="zoom-up" style="margin-top: -27px;">
                            <p class="chakra-text css-17jzmug" style="text-align: center">
                                Tỷ trọng áp đảo của khối FDI trong tổng kim ngạch xuất khẩu


                            </p>
                        </div>


                        <div data-aos="zoom-up" >
                            <p >
                                Ông Vũ Thành Tự Anh (Đại học Fulbright Việt Nam) là một trong những chuyên gia đã có
                                nhiều bình luận liên quan vấn đề này. Theo ông Tự Anh, việc nền kinh tế phụ thuộc ngày
                                càng nhiều vào khu vực FDI là điều rất khó chấp nhận. “Nếu muốn tạo ra nội lực, thì
                                không thể phụ thuộc vào FDI như hiện nay”, ông Tự Anh nói.
                            </p>
                            <div class="chakra-stack css-1o07fwd" style="margin-top: 3px">
                                <div data-aos="zoom-up"  style="margin-top: 30px;">
                                    <img src="6.png" alt="Hình ảnh" style="width: 100%">
                                </div>
                            </div>

                            <p  style="margin-top: 15px;">


                                Điều này đặt ra bài toán “khó” dành cho các doanh nghiệp trong nước phải xây dựng thương
                                hiệu mạnh hơn về công nghiệp cho riêng mình.

                                Bên cạnh đó, việc các doanh nghiệp FDI liên tục “áp đảo” xuất khẩu đã, đang và sẽ tiếp
                                tục đặt ra không ít lo ngại cho xuất khẩu bền vững của Việt Nam.

                            </p>
                        </div>
                        {{-- biểu đồ 7 --}}
                        <div class="chakra-stack css-3ir41n">
                            <div id="chart-container7" style="width:100%"></div>

                            <div data-aos="zoom-up" style="margin-top: 9px;">
                                <p class="chakra-text css-17jzmug" style="text-align: center">
                                    Kim ngạch xuất/nhập siêu của khu vực trong nước và FDI (đơn vị: tỷ USD)
                                </p>
                            </div>
                        </div>

                        <style>
                            .chakra-stack.css-3ir41n {
                                position: relative; /* Đảm bảo phần tử cha có position để xác định kích thước cho phần tử con */
                                width: 100%; /* Đảm bảo phần tử cha rộng 100% để thích nghi với kích thước màn hình */
                            }

                            /* Đảm bảo biểu đồ rộng 100% của phần tử chứa nó */
                            #chart-container7 {
                                width: 100%;
                            }
                        </style>
                        <div data-aos="zoom-up" >
                            <p >
                                Nhìn lại quá trình xuất khẩu xuyên suốt có thể thấy, năm 2018 là năm đầu tiên, doanh
                                nghiệp trong nước tăng trưởng xuất khẩu cao hơn khối doanh nghiệp FDI. Lần đầu tiên, xu
                                hướng liên tục giảm về tỷ trọng của doanh nghiệp trong nước trong xuất khẩu so với doanh
                                nghiệp FDI tạm thời đảo chiều. Tỷ trọng của doanh nghiệp FDI liên tục tăng trong nhiều
                                năm thì lần đầu tiên cũng đã tạm dừng lại, giảm xuống. Đây là điều rất đáng mừng. Kết
                                quả đảo chiều kể trên, một phần do nỗ lực của chính doanh nghiệp, một phần do các giải
                                pháp của Chính phủ trong cải thiện môi trường kinh doanh, tháo gỡ khó khăn, nâng năng
                                lực cạnh tranh, tạo thuận lợi cho doanh nghiệp.
                            </p>
                            <p >

                                Tuy nhiên, những tín hiệu tích ấy chưa đạt sự ổn định và không kéo dài quá lâu. Bằng
                                chứng là ngay năm 2020, trong tổng kim ngạch xuất khẩu hàng hóa đạt đạt 281,5 tỷ USD,
                                khu vực kinh tế trong nước chỉ đóng góp 78,2 tỷ USD, giảm 1,1%, chiếm 27,8% tổng kim
                                ngạch xuất khẩu; còn khu vực có vốn đầu tư nước ngoài (kể cả dầu thô) đạt 203,3 tỷ USD,
                                tăng 9,7%, vươn lên chiếm 72,2%. Năm 2022, xuất khẩu toàn nền kinh tế đạt 371,5 tỷ USD,
                                xuất siêu 11,2 tỷ USD, trong đó, khối doanh nghiệp FDI đóng góp tới 275,9 tỷ USD (tương
                                ứng 74% tổng kim ngạch xuất khẩu), còn kim ngạch xuất khẩu của khu vực doanh nghiệp 100%
                                vốn trong nước chỉ tăng 6,8%.
                            </p>
                            <p >

                                Những năm gần đây, Việt Nam liên tục xuất siêu với mức xuất siêu năm sau cao hơn năm
                                trước. Tuy nhiên, điểm đáng lo ngại là Việt Nam xuất siêu cao chủ yếu do doanh nghiệp
                                FDI.
                            </p>
                            <p >

                                Chuyên gia Lê Quốc Phương đánh giá, thời gian qua, doanh nghiệp Việt có cải thiện về tỷ
                                trọng xuất khẩu nhưng quá chậm, chưa ổn định. Điểm yếu cố hữu của doanh nghiệp nội vẫn
                                là nhập siêu. Từ đó cho thấy xu thế cải thiện của doanh nghiệp Việt trong cơ cấu xuất
                                khẩu chưa đủ để đánh giá bền vững, còn phải nỗ lực nhiều. Chính phủ phải hỗ trợ rất
                                nhiều, chính sách phải phù hợp trong khuôn khổ các cam kết quốc tế. Ngoài ra, bản thân
                                doanh nghiệp cũng phải nỗ lực để cải thiện vị thế trong xuất khẩu.

                            </p>
                        </div>
                        <div data-aos="fade-up" data-aos-offset="200">
                            <h2 class="chakra-heading css-kjiv5n font-style-data">
                                Chiến lược hướng tới xuất nhập khẩu bền vững
                            </h2>

                        </div>
                        <div data-aos="zoom-up" >
                            <p >
                                Nhìn nhận thẳng thắn vào các mặt còn hạn chế, Bộ Công Thương đã đề ra chiến lược xuất
                                nhập khẩu cho giai đoạn mới, đến năm 2030 đặt mục tiêu có sự quan tâm đúng mức đến chất
                                lượng tăng trưởng và tính bền vững của phát triển xuất nhập khẩu. Đó là xuất khẩu, nhập
                                khẩu tăng trưởng ổn định, cán cân thương mại lành mạnh, hợp lý với tốc độ tăng trưởng
                                xuất khẩu hàng hóa bình quân 6-7%/năm trong thời kỳ 2021-2030, trong đó giai đoạn
                                2021-2025 tăng trưởng xuất khẩu bình quân 8-9%/năm; giai đoạn 2026-2030 tăng trưởng bình
                                quân 5-6%/năm; tốc độ tăng trưởng nhập khẩu hàng hóa bình quân 5-6%/năm trong thời kỳ
                                2021-2030, trong đó giai đoạn 2021-2025 tăng trưởng nhập khẩu bình quân 7-8%/năm; giai
                                đoạn 2026-2030 tăng trưởng bình quân 4-5%/năm. Cân bằng cán cân thương mại trong giai
                                đoạn 2021-2025, tiến tới duy trì thặng dư thương mại bền vững giai đoạn 2026-2030; hướng
                                đến cán cân thương mại lành mạnh, hợp lý với các đối tác thương mại chủ chốt.
                            </p>
                            <p >

                                Theo đó, mục tiêu hoạt động xuất nhập khẩu hướng tới là phát triển xuất nhập khẩu hàng
                                hóa theo chiều sâu. Tập trung ưu tiên phát triển xuất khẩu các mặt hàng có quy mô xuất
                                khẩu lớn, lợi thế cạnh tranh cao (điện tử, dệt may, da giày, nông sản, đồ gỗ..) gắn với
                                đa dạng hóa và nâng cao chất lượng sản phẩm xuất khẩu. Riêng với nhóm hàng công nghiệp
                                chế biến, chế tạo, cần gia tăng tỷ trọng xuất khẩu hàng hóa có hàm lượng chế biến sâu,
                                công nghệ cao, có giá trị gia tăng cao, tỷ lệ nội địa hoá lớn, đáp ứng tiêu chuẩn cao về
                                chất lượng và phát triển bền vững của các thị trường.


                            </p>
                        </div>
                        <div class="chakra-stack css-1o07fwd" style="margin-top: 3px">
                            <div data-aos="zoom-up"  style="margin-top: 30px;">
                                <img src="2.png" alt="Hình ảnh" style="width: 100%">
                            </div>
                        </div>
                        <div data-aos="zoom-up" >
                            <p >
                                Để không phụ thuộc quá lớn vào một số thị trường và giảm thiểu ảnh hưởng tiêu cực từ các
                                cuộc xung đột thương mại, Bộ Công Thương xác định sẽ chú trọng mở rộng thị trường xuất
                                khẩu. Trong đó, đối với thị trường châu Á – châu Phi, duy trì tăng trưởng xuất khẩu ở
                                mức cao hơn so với nhập khẩu và kiểm soát nhập siêu từ các thị trường châu Á và đối với
                                các mặt hàng công nghiệp chế biến, chế tạo, nông sản. Phấn đấu đạt tỷ trọng thị trường
                                xuất khẩu vào khu vực châu Á vào khoảng 49-50% vào năm 2025 và 46-47% vào năm 2030.
                            </p>

                            <p >
                                Đối với thị trường châu Âu, đẩy mạnh xuất khẩu nhằm duy trì vững chắc và mở rộng thị
                                phần
                                xuất khẩu tại các thị trường Đức, Pháp, Hà Lan, Anh, Ý, Thụy Sỹ, Tây Ban Nha và các
                                nước thành viên Liên minh kinh tế Á - Âu (EAEU). Đồng thời đẩy mạnh xuất khẩu sang thị trường
                                các nước EAEU nhóm hàng thực phẩm, gạo, dệt may, đồ da, đồ gỗ, thủy sản, hàng điện tử,
                                điện thoại gắn với nâng cao chất lượng, tiêu chuẩn sản phẩm. Từ đó, tăng tỷ trọng thị
                                trường xuất khẩu vào khu vực châu Âu lên 16-17% tổng kim ngạch xuất khẩu vào năm 2025;
                                18-19% vào năm 2030.
                            </p>

                            <p >
                                Đối với thị trường châu Mỹ, tiếp tục củng cố và mở rộng thị phần xuất khẩu tại thị
                                trường Hoa Kỳ, Canada và Mexico những nhóm hàng có thế mạnh xuất khẩu của Việt Nam. Thúc
                                đẩy xuất khẩu nhóm hàng giày dép, ba lô, túi xách, nông sản, hàng thủ công mỹ nghệ, dệt
                                may, điện, điện tử, cơ khí, động cơ điện, thiết bị máy móc, đồ gỗ vào khu vực Mỹ Latinh.
                                Phấn đấu đến năm 2025, tỷ trọng xuất khẩu vào khu vực châu Mỹ lên 32-33% tổng kim ngạch
                                xuất khẩu và 33-34% vào năm 2030.
                            </p>
                        </div>

                        <div class="chakra-stack css-1o07fwd" style="margin-top: 3px">
                            <div data-aos="zoom-up"  style="margin-top: 30px;">
                                <img src="3.png" alt="Hình ảnh" style="width: 100%">
                            </div>
                        </div>
                        <div data-aos="zoom-up" >
                            <p >
                                Song song với việc mở rộng thị trường xuất khẩu, Bộ Công Thương cũng xác định sẽ đa dạng
                                hóa thị trường nhập khẩu, đặc biệt là thị trường nhập khẩu nguyên phụ liệu phục vụ sản
                                xuất, hạn chế phụ thuộc lớn vào một thị trường; từng bước cải thiện cán cân thương mại
                                với các thị trường Việt Nam nhập siêu theo hướng cân bằng hơn.


                            </p>
                        </div>
                        <div class="chakra-stack css-1o07fwd" style="margin-top: 3px">
                            <div data-aos="zoom-up"  style="margin-top: 30px;">
                                <img src="4.png" alt="Hình ảnh" style="width: 100%">
                            </div>
                        </div>
                        <div data-aos="zoom-up" >
                            <p >
                                Với mong muốn hiện thực hóa mục tiêu xuất khẩu hàng hóa bền vững đi liền với đa dạng hóa
                                thị trường hướng đến cán cân thương mại lành mạnh, hợp lý với các đối tác, theo bà
                                Nguyễn Cẩm Trang - Phó Cục trưởng Cục Xuất, nhập khẩu (Bộ Công Thương) với vai trò là cơ
                                quan thường trực, Bộ Công Thương có chia sẻ một số những giải pháp như sau:
                            </p>
                            <p >
                                “Thủ tướng Chính phủ đã giao Bộ Công Thương phối hợp với các bộ, ngành xây dựng Chương
                                trình hành động thực hiện chiến lược, ban hành vào đầu quý III-2022. Bộ Công Thương tiếp
                                tục khai thác hiệu quả các hiệp định thương mại tự do, mở rộng và đa dạng thị trường
                                xuất khẩu, hỗ trợ doanh nghiệp tận dụng cam kết trong các hiệp định thương mại tự do thế
                                hệ mới. Theo dõi sát biến động của kinh tế thế giới và trong nước, thông tin dự báo tình
                                hình thị trường hàng hóa, chủ động đánh giá các tác động đến sản xuất; xuất, nhập khẩu
                                của Việt Nam để kịp thời ứng phó, giúp doanh nghiệp thâm nhập thị trường hiệu quả.
                            </p>
                            <p >
                                Mặt khác, Bộ tiếp tục đơn giản hóa thủ tục hành chính, thúc đẩy phát triển dịch vụ
                                logistics góp phần nâng cao năng lực cạnh tranh của hàng hóa Việt Nam. Đẩy mạnh xúc tiến
                                thương mại trực tuyến; nâng cao năng lực xúc tiến thương mại trên môi trường số cho cộng
                                đồng doanh nghiệp...
                            </p>
                            <p >
                                Định kỳ 5 năm tổ chức đánh giá việc thực hiện chiến lược; đề xuất trình Thủ tướng Chính
                                phủ quyết định điều chỉnh mục tiêu, nội dung trong trường hợp cần thiết.”
                            </p>
                        </div>
                    </div>
                </div>

            </div>
            @if(\Agent::isMobile())
                <div class="chakra-stack css-3ir41n">
                    <div data-aos="zoom-in">
                        <img class="chakra-image css-wayys6" src="anhnen.jpg"/>
                    </div>

                </div>
            @else
                <div data-aos="fade-down-right" data-aos-offset="400"
                     style="background-image:url(anhnen.jpg);background-size:cover;background-position: right"
                     class="css-1yiqmvn">


                </div>
            @endif


            <div class="chakra-stack css-1o07fwd" style="margin-top: 3px">

                <div data-aos="zoom-up" >
                    <p >
                        Nhập khẩu và xuất khẩu đóng vai trò quan trọng trong việc định hình nền kinh tế của mỗi
                        quốc gia. Trong bối cảnh thế giới ngày càng chú trọng đến phát triển bền vững, Việt Nam
                        đang nỗ lực hướng tới một hệ thống xuất nhập khẩu bền vững để đảm bảo sự cân bằng giữa
                        sự phát triển kinh tế và bảo vệ môi trường.
                    </p>
                    <p >
                        Một trong những nguyên tắc cơ bản của xuất nhập khẩu bền vững là việc giảm thiểu tác
                        động tiêu cực đối với môi trường và xã hội. Vì thế, Việt Nam đang chú trọng vào việc
                        thúc đẩy xuất khẩu các sản phẩm mang tính bền vững, như sản phẩm hữu cơ, sản phẩm tái
                        chế, và các sản phẩm không gây ảnh hưởng lớn đến nguồn tài nguyên tự nhiên.
                    </p>
                    <p >
                        Để thực hiện hóa mục tiêu xuất nhập khẩu bền vững, hiện nay Việt Nam đã và đang tăng
                        cường sản xuất và xuất khẩu các sản phẩm nông sản hữu cơ. Việc này không chỉ tăng cường
                        giá trị thương mại cho đất nước mà còn giúp bảo vệ môi trường, giảm sử dụng hóa chất độc
                        hại và tăng cường sức khỏe cho người tiêu dùng. Điều này thể hiện cam kết của Việt Nam
                        trong việc xây dựng một nền kinh tế xanh và bền vững.
                    </p>
                </div>
            </div>


            <div class="css-1k9oxz7" data-aos="fade-left" data-aos-offset="500" data-aos-duration="500"
                 >
                @if(\Agent::isMobile())
                    <div data-aos="zoom-up" data-aos-offset="400" >
                        <div class="chakra-stack css-vkn65q">
                            <div class="swiper cube-swiper swiper-3d">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <img class="chakra-image css-1ncscnu" src="anh1.jpg"/>
                                    </div>
                                    <div class="swiper-slide">
                                        <img class="chakra-image css-1ncscnu" src="anh2.jpg"/>
                                    </div>
                                    <div class="swiper-slide">
                                        <img class="chakra-image css-1ncscnu" src="anh3.jpg"/>
                                    </div>
                                    <div class="swiper-slide">
                                        <img class="chakra-image css-1ncscnu" src="anh4.jpg"/>
                                    </div>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                            <p class="chakra-text css-17jzmug"> Sản phẩm của Việt Nam đáp ứng thị hiếu khách hàng và các
                                tiêu chuẩn kỹ thuật, quy định của thị trường
                                quốc tế (Ảnh: Nhóm 6 )i</p>
                        </div>
                    </div>
                @else
                    <div class="swiper carousel-swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img class="chakra-image css-1ncscnu" src="anh1.jpg"/>
                            </div>
                            <div class="swiper-slide">
                                <img class="chakra-image css-1ncscnu" src="anh2.jpg"/>
                            </div>
                            <div class="swiper-slide">
                                <img class="chakra-image css-1ncscnu" src="anh3.jpg"/>
                            </div>
                            <div class="swiper-slide">
                                <img class="chakra-image css-1ncscnu" src="anh4.jpg"/>
                            </div>

                        </div>
                        <div class="swiper-pagination"></div>
                        <p class="chakra-text css-17jzmug">
                            Một số sản phẩm xuất khẩu của Việt Nam (Ảnh: Nhóm 6)

                        </p>
                    </div>
                @endif

            </div>


            <div class="chakra-stack css-1o07fwd" style="margin-top: 3px">
                <div data-aos="zoom-up" >
                    <p >
                        Ngoài ra, việc thúc đẩy xuất khẩu sản phẩm tái chế cũng là một bước quan trọng trong
                        chiến lược xuất khẩu bền vững của Việt Nam. Việc tái chế và sử dụng nguyên liệu tái chế
                        không chỉ giúp giảm lượng rác thải mà còn giảm áp lực đối với tài nguyên tự nhiên. Các
                        sản phẩm tái chế từ Việt Nam ngày càng được đánh giá cao trên thị trường thế giới với sự
                        đa dạng và chất lượng.

                    </p>
                    <p >
                        Tính bền vững cũng được thể hiện thông qua việc Việt Nam chú trọng vào các nguồn năng
                        lượng sạch trong sản xuất và xuất khẩu. Việc áp dụng công nghệ tiên tiến và sử dụng
                        nguồn năng lượng tái tạo không chỉ giảm phát thải khí nhà kính mà còn tạo ra những sản
                        phẩm xuất khẩu có giá trị thêm cao.
                    </p>
                </div>
            </div>


            <div class="css-1k9oxz7" data-aos="fade-left" data-aos-offset="500" data-aos-duration="500"
                 >
                @if(\Agent::isMobile())
                    <div data-aos="zoom-up" data-aos-offset="400" >
                        <div class="chakra-stack css-vkn65q">
                            <div class="swiper cube-swiper swiper-3d">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <img class="chakra-image css-1ncscnu" src="anh5.jpg"/>
                                    </div>
                                    <div class="swiper-slide">
                                        <img class="chakra-image css-1ncscnu" src="anh6.jpg"/>
                                    </div>
                                    <div class="swiper-slide">
                                        <img class="chakra-image css-1ncscnu" src="anh7.jpg"/>
                                    </div>
                                    <div class="swiper-slide">
                                        <img class="chakra-image css-1ncscnu" src="anh8.jpg"/>
                                    </div>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                            <p class="chakra-text css-17jzmug"> Sản phẩm của Việt Nam đáp ứng thị hiếu khách hàng và các
                                tiêu chuẩn kỹ thuật, quy định của thị trường
                                quốc tế (Ảnh: Sưu tầm)</p>
                        </div>
                    </div>

                @else

                    <div class="swiper carousel-swiper">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img class="chakra-image css-1ncscnu" src="anh5.jpg"/>
                            </div>
                            <div class="swiper-slide">
                                <img class="chakra-image css-1ncscnu" src="anh6.jpg"/>
                            </div>
                            <div class="swiper-slide">
                                <img class="chakra-image css-1ncscnu" src="anh7.jpg"/>
                            </div>
                            <div class="swiper-slide">
                                <img class="chakra-image css-1ncscnu" src="anh8.jpg"/>
                            </div>

                        </div>
                        <div class="swiper-pagination"></div>
                        <p class="chakra-text css-17jzmug">
                            Sản phẩm của Việt Nam đáp ứng thị hiếu khách hàng và các tiêu chuẩn kỹ thuật, quy định của
                            thị
                            trường
                            quốc tế (Ảnh: Sưu tầm)

                        </p>
                    </div>

                @endif

            </div>

            <div class="chakra-stack css-1o07fwd" style="margin-top: 3px">
                <div data-aos="zoom-up" >
                    <p >
                        Bên cạnh đó, chính sách và hệ thống quy định của Việt Nam cũng đang dần điều chỉnh để
                        khuyến khích doanh nghiệp thực hiện xuất nhập khẩu bền vững. Các chính sách thuế và
                        khuyến khích đầu tư đang được xem xét để tạo điều kiện thuận lợi cho các doanh nghiệp
                        hướng tới sự phát triển bền vững.
                    </p>
                    <p >
                        Tựu chung lại, Việt Nam đã và đang có những bước tiến mạnh mẽ hướng tới xuất nhập khẩu
                        bền vững, không chỉ tạo ra cơ hội kinh doanh mới mà còn đảm bảo bền vững cho môi trường
                        và xã hội. Những nỗ lực này chính là bước quan trọng để Việt Nam thực hiện cam kết với
                        cộng đồng quốc tế và đồng thời góp phần vào sự phát triển bền vững của thế giới.


                    </p>
                </div>
                <div class="chakra-stack css-gboczk">
                    <p class="chakra-text css-722v25"><b>Thực hiện : </b> Nhóm 6 - Thanh Long, Duy Long, Hải
                        Ngân, Quỳnh Phương, Phương Thảo
                    </p>
                </div>
            </div>

        </div>
    </div>


    <style>
        /* CSS cho map và biểu đồ */
        #map {
            height: 300px;
        }

    </style>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Lobster&display=swap');

        .font-style-data {
            font-family: 'Lobster', cursive; /* Sử dụng Lobster font */
            text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);
        }

        .icon-text {
            display: flex;
            align-items: center; /* Căn các phần tử theo trục dọc */
            margin-top: 6px;
        }

        .icon-text img {
            margin-right: 5px; /* Khoảng cách giữa hình ảnh và chữ */
        }

        .icon-text i {
            font-size: 12px;
        }

    </style>


    <style>
        .chart-container-pie .canvasjs-chart-credit {
            display: none;
        }

        /* CSS */
        /* CSS */
        .charts-container {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            max-width: 100%;
            /* Đảm bảo không tràn bố cục */
        }

        .chart {
            flex: 0 0 calc(33.33% - 20px);
            /* Điều chỉnh kích thước của mỗi đồ thị */
            margin-bottom: 20px;
            /* Khoảng cách giữa các đồ thị */
            box-sizing: border-box;
            /* Đảm bảo padding và border không làm thay đổi kích thước của phần tử */
            padding: 10px;
            /* Thêm padding để tạo khoảng cách xung quanh đồ thị */
        }

        /* Điều chỉnh media query nếu cần thiết cho responsive */
        @media screen and (max-width: 768px) {
            .chart {
                flex: 0 0 calc(50% - 20px);
                /* Điều chỉnh kích thước khi màn hình thu nhỏ */
            }
        }

        #change_name {

        }
    </style>


    <!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> -->
    {{-- <script src="https://cdn.jsdelivr.net/npm/chart.js"></script> --}}
    {{-- // biểu đồ 2 --}}
    <script>
        // Dữ liệu xuất nhập khẩu
        const years1 = ['2018', '2019', '2020', '2021', '2022', '2023 (6 tháng)'];
        const exportsData = [244.72, 263.45, 281.5, 336.25, 371.85, 164.68];
        const importsData = [237.51, 253.51, 262.4, 332.25, 360.65, 151.83];

        // Tạo biểu đồ cột ngang
        Highcharts.chart('chart-container-data', {
            chart: {
                type: 'bar' // Loại biểu đồ: cột ngang
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years1,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            plotOptions: {
                bar: {
                    stacking: 'normal'
                }
            },
            series: [{
                name: 'Xuất khẩu',
                data: exportsData,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột xuất khẩu
            },
                {
                    name: 'Nhập khẩu',
                    data: importsData,
                    color: 'rgba(255, 144, 14, 0.8)' // Màu của cột nhập khẩu
                }
            ]
        });
    </script>
    {{-- //biểu đồ 3 --}}
    <script>
        // Dữ liệu cho từng quốc gia
        const years3 = ['2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022',
            '2023 (6 tháng)'
        ];
        const detmay = [17.701, 21.399, 25.017, 27.209, 28.664, 31.88, 36.932, 39.613, 35.014, 41.14, 45.387, 15.729];
        const go = [4.666, 5.591, 6.23, 6.892, 6.965, 7.703, 8.908, 10.652, 12.371, 14.809, 16.012, 6.056];
        const caosu = [2.86, 2.487, 1.781, 1.532, 1.67, 2.25, 2.092, 2.301, 2.38, 3.279, 3.316, 1.048];
        const coffee = [3.673, 2.718, 3.557, 2.672, 3.335, 3.501, 3.365, 2.863, 2.74, 3.073, 4.056, 1.006];
        const fe = [1.642, 1.776, 1.998, 1.685, 2.03, 3.148, 4.548, 4.205, 4.909, 11.79, 7.994, 4.257];
        const fish = [6.089, 6.692, 7.826, 6.569, 7.048, 8.31, 8.788, 8.542, 8.41, 8.883, 10.924, 4.146];
        const rice = [3.673, 2.923, 2.936, 2.799, 2.159, 2.634, 3.061, 2.807, 3.12, 3.288, 3.455, 2.256];
        const rau = [0.827, 1.074, 1.489, 1.84, 2.458, 3.501, 3.806, 3.746, 3.27, 3.547, 3.365, 2.68];
        const deo = [1.596, 1.818, 2.045, 2.075, 2.213, 2.548, 3.46, 3.437, 3.448, 4.931, 5.494, 2.385];
        const productFe = [1.377, 1.567, 1.734, 1.773, 1.985, 2.298, 3.015, 3.317, 2.891, 3.952, 4.652, 2.046];
        // Thêm dữ liệu cho các quốc gia khác

        // Tạo biểu đồ cột cho từng quốc gia
        Highcharts.chart('detmay-container', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years3,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Kim ngạch xuất khẩu ngành dệt may ',
                data: detmay,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });


        Highcharts.chart('go-container', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: years3,
                title: {
                    text: ''
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Kim ngạch xuất khẩu Gỗ và sản phẩm từ gỗ',
                data: go,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('caosu-container', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: years3,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Kim ngạch xuất khẩu cao su',
                data: caosu,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('coffee-container', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years3,
                title: {
                    text: ''
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Kim ngạch xuất khẩu Cà phê',
                data: coffee,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('fe-container', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years3,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: ' Kim ngạch xuất khẩu ngành sắt thép',
                data: fe,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('fish-container', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years3,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: ' Kim ngạch xuất khẩu ngành thủy sản',
                data: fish,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('rice-container', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years3,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: ' Kim ngạch xuất khẩu gạo',
                data: rice,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('rau-container', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years3,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Kim ngạch xuất khẩu Rau quả ',
                data: rau,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('deo-container', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years3,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Kim ngạch xuất khẩu sản phẩm từ chất dẻo ',
                data: deo,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('productFe-container', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years3,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Kim ngạch xuất khẩu sản phẩm từ sắt thép',
                data: productFe,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });


        document.querySelectorAll('.custom-select .option').forEach(option => {
            option.addEventListener('click', function () {
                const selectedValue = this.getAttribute('data-value');

                // Xóa lớp active khỏi tất cả các tùy chọn
                document.querySelectorAll('.custom-select .option').forEach(opt => {
                    opt.classList.remove('active');
                });

                // Hiển thị container của tùy chọn được chọn
                const allContainers = document.querySelectorAll('[id$="-container"]');
                allContainers.forEach(container => {
                    container.style.display = 'none';
                });
                document.getElementById(selectedValue + '-container').style.display = 'block';

                // Thêm lớp active cho tùy chọn được chọn
                this.classList.add('active');

                switch (selectedValue) {
                    // Xử lý các tùy chọn khác nếu cần thiết
                }
            });
        });

        document.querySelectorAll('.custom-select-country .option').forEach(option => {
            option.addEventListener('click', function () {
                const selectedValue = this.getAttribute('data-value');

                // Xóa lớp active khỏi tất cả các tùy chọn
                document.querySelectorAll('.custom-select-country .option').forEach(opt => {
                    opt.classList.remove('active');
                });

                // Hiển thị container của tùy chọn được chọn
                const allContainers = document.querySelectorAll('[id$="-container-country"]');
                allContainers.forEach(container => {
                    container.style.display = 'none';
                });
                document.getElementById(selectedValue + '-container-country').style.display = 'block';

                // Thêm lớp active cho tùy chọn được chọn
                this.classList.add('active');

                switch (selectedValue) {
                    case 'eng':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Anh');
                        break;
                    case 'japan':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Nhật Bản');

                        break;
                    case 'hongkong':
                        $('#change_name').html('Kim ngạch xuất khẩu sang HongKong');

                        break;
                    case 'southkorea':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Hàn Quốc');

                        break;
                    case 'china':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Trung Quốc');

                        break;
                    case 'thailand':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Thái Lan');

                        break;
                    case 'india':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Ấn Độ');

                        break;
                    case 'germany':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Đức');

                        break;
                    case 'netherlands':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Hà Lan');

                        break;
                    case 'amarica':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Mỹ');

                        break;
                    default:
                        break;
                }
            });
        });

        // Thêm biểu đồ cho các quốc gia khác tương tự như trên
    </script>
    {{-- // biểu đồ 4 --}}
    <script>

        var map = L.map('map', {
            zoomControl: false, // Tắt nút điều khiển phóng to/thu nhỏ
            doubleClickZoom: false, // Ngăn phóng to khi double click
            scrollWheelZoom: false, // Ngăn phóng to bằng scroll chuột
            boxZoom: false, // Ngăn phóng to bằng cách chọn một vùng trên map
            keyboard: false, // Ngăn điều khiển bằng bàn phím
        }).setView([0, 0], 2);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
        }).addTo(map);

        // Dữ liệu cho từng quốc gia
        const years4 = ['2018', '2019', '2020', '2021', '2022', '2023 (6 tháng)'];
        const vietnamData = [244.72, 263.45, 281.5, 336.25, 371.85, 164.68];
        const ukData = [5.78, 5.75, 4.95, 5.76, 6.06, 2.86];
        const japanData = [18.82, 20.33, 19.28, 20.13, 24.23, 11.06];
        const hongKongData = [7.96, 7.15, 10.43, 12, 10.93, 4.22];
        const southKoreaData = [18.24, 19.73, 19.10, 21.94, 24.29, 11.05];
        const chinaData = [41.36, 41.46, 48.90, 55.92, 57.70, 25.92];
        const thailandData = [5.48, 5.30, 4.91, 6.15, 7.47, 3.55];
        const indiaData = [6.54, 6.67, 5.23, 6.28, 7.96, 3.89];
        const germanyData = [6.87, 6.55, 6.64, 7.28, 8.96, 3.69];
        const netherlandsData = [7.1, 6.88, 7, 7.68, 10.43, 4.83];
        const usaData = [47.53, 61.33, 77.07, 96.27, 109.39, 44.42];
        // Thêm dữ liệu cho các quốc gia khác

        var chartData = {
            VN: {
                containerId: "VN-container-data-chart",
                title: "TỔNG KIM NGẠCH XUẤT KHẨU CỦA VIỆT NAM (đơn vị: tỷ USD)",
                dataChart: vietnamData
                // Thêm dữ liệu biểu đồ cho Việt Nam ở đây
            },

            GB: {
                containerId: "GB-container-data",
                title: "Kim ngạch xuất khẩu sang Anh",
                dataChart: ukData
                // Thêm dữ liệu biểu đồ cho Anh ở đây
            },

            JP: {
                containerId: "JP-container-data",
                title: "Kim ngạch xuất khẩu sang Nhật",
                dataChart: japanData
            },

            HK: {
                containerId: "HK-container-data",
                title: "Kim ngạch xuất khẩu sang Hồng Kông",
                dataChart: hongKongData
                // Thêm dữ liệu biểu đồ cho Hồng Kông ở đây
            },

            KR: {
                containerId: "KR-container-data",
                title: "TỔNG KIM NGẠCH XUẤT KHẨU CỦA HÀN QUỐC (đơn vị: tỷ USD)",
                dataChart: southKoreaData
                // Thêm dữ liệu biểu đồ cho Việt Nam ở đây
            },

            US: {
                containerId: "GB-container-data",
                title: "Kim ngạch xuất khẩu sang Anh",
                dataChart: usaData
                // Thêm dữ liệu biểu đồ cho Anh ở đây
            },

            CN: {
                containerId: "CN-container-data",
                title: "Kim ngạch xuất khẩu sang Trung quốc",
                dataChart: chinaData
                // Thêm dữ liệu biểu đồ cho Nhật ở đây
            },

            DE: {
                containerId: "DE-container-data",
                title: "Kim ngạch xuất khẩu sang Đức",
                dataChart: germanyData
                // Thêm dữ liệu biểu đồ cho Hồng Kông ở đây
            },

            NL: {
                containerId: "NL-container-data",
                title: "Kim ngạch xuất khẩu sang Hà Lan",
                dataChart: netherlandsData
                // Thêm dữ liệu biểu đồ cho Hồng Kông ở đây
            },

            IN: {
                containerId: "IN-container-data",
                title: "Kim ngạch xuất khẩu sang Ấn độ",
                dataChart: indiaData
                // Thêm dữ liệu biểu đồ cho Hồng Kông ở đây
            },

            TH: {
                containerId: "TH-container-data",
                title: "Kim ngạch xuất khẩu sang Thái Lan",
                dataChart: thailandData
                // Thêm dữ liệu biểu đồ cho Hồng Kông ở đây
            },

            // Thêm dữ liệu cho các quốc gia khác
        };

        var countries = [
            {name: 'Vietnam', code: 'VN', latlng: [14.0583, 108.2772]},
            {name: 'USA', code: 'US', latlng: [37.0902, -95.7129]},
            {name: 'Japan', code: 'JP', latlng: [36.2048, 138.2529]}, // Thêm thông tin cho Nhật Bản
            {name: 'Hong Kong', code: 'HK', latlng: [22.3193, 114.1694]},// Thêm thông tin cho Hồng Kông
            {name: 'United Kingdom', code: 'GB', latlng: [55.3781, -3.4360]}, // Vương Quốc Anh
            {name: 'South Korea', code: 'KR', latlng: [35.9078, 127.7669]}, // Hàn Quốc
            {name: 'China', code: 'CN', latlng: [35.8617, 104.1954]}, // Trung Quốc
            {name: 'Thailand', code: 'TH', latlng: [15.8700, 100.9925]}, // Thái Lan
            {name: 'India', code: 'IN', latlng: [20.5937, 78.9629]}, // Ấn Độ
            {name: 'Netherlands', code: 'NL', latlng: [52.1326, 5.2913]}, // Hà Lan
            {name: 'Germany', code: 'DE', latlng: [51.1657, 10.4515]} // Đức

            // Thêm các quốc gia khác
        ];


        countries.forEach(country => {
            var icon = L.icon({
                iconUrl: `https://flagcdn.com/24x18/${country.code.toLowerCase()}.png`,
                iconSize: [24, 18],
                iconAnchor: [12, 9],
            });

            var marker = L.marker(country.latlng, {icon: icon}).addTo(map);

            marker.on('click', function () {
                var currentCountry = country.code;

                const allContainers = document.querySelectorAll('[id$="-container-data"]');
                allContainers.forEach(container => {
                    container.style.display = 'none';
                });


                document.getElementById(currentCountry + '-container-data').style.display = 'block';

                switch (currentCountry) {
                    case 'GB':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Anh');
                        break;
                    case 'JP':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Nhật Bản');

                        break;
                    case 'HK':
                        $('#change_name').html('Kim ngạch xuất khẩu sang HongKong');

                        break;
                    case 'KR':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Hàn Quốc');

                        break;
                    case 'CN':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Trung Quốc');

                        break;
                    case 'TH':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Thái Lan');

                        break;
                    case 'IN':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Ấn Độ');

                        break;
                    case 'DE':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Đức');

                        break;
                    case 'NL':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Hà Lan');

                        break;
                    case 'US':
                        $('#change_name').html('Kim ngạch xuất khẩu sang Mỹ');

                        break;
                    default:
                        break;
                }
            });
        });


        // Tạo biểu đồ cột cho từng quốc gia
        Highcharts.chart('VN-container-data-chart', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years4,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            series: [{
                name: 'Việt Nam',
                data: vietnamData,
                color: 'rgba(232,6,36,0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('GB-container-data', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years4,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Vương Quốc Anh',
                data: ukData,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('JP-container-data', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years4,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Nhật Bản',
                data: japanData,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('HK-container-data', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years4,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'HongKong',
                data: hongKongData,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('KR-container-data', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years4,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Hàn Quốc',
                data: southKoreaData,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('CN-container-data', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years4,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Trung Quốc',
                data: chinaData,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('TH-container-data', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years4,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Thái Lan',
                data: thailandData,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('IN-container-data', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years4,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Ấn Độ',
                data: indiaData,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('DE-container-data', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years4,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Đức',
                data: germanyData,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('NL-container-data', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years4,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Hà Lan',
                data: netherlandsData,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        Highcharts.chart('US-container-data', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years4,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            series: [{
                name: 'Mỹ',
                data: usaData,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột
            }]
        });

        document.getElementById('country-selector4').addEventListener('change', function () {
            const selectedCountry = this.value;

            // Ẩn tất cả container
            const allContainers = document.querySelectorAll('[id$="-container-country"]');
            allContainers.forEach(container => {
                container.style.display = 'none';
            });

            // Hiển thị container của quốc gia được chọn
            document.getElementById(selectedCountry + '-container-country').style.display = 'block';

            // Cập nhật biểu đồ nếu cần thiết
            switch (selectedCountry) {
                case 'hongkong':
                    // Cập nhật biểu đồ cho Hồng Kông (TQ) nếu cần thiết
                    break;
                case 'southkorea':
                    // Cập nhật biểu đồ cho Hàn Quốc nếu cần thiết
                    break;
                case 'china':
                    // Cập nhật biểu đồ cho Trung Quốc nếu cần thiết
                    break;
                // Thêm các trường hợp cho các quốc gia khác nếu cần thiết
            }
        });
        // Thêm biểu đồ cho các quốc gia khác tương tự như trên
    </script>
    {{-- biểu đồ 5 --}}
    <script>
        // Dữ liệu xuất nhập khẩu và mức độ tăng trưởng
        const years5 = ['2018', '2019', '2020', '2021', '2022', '2023 (6 tháng)'];
        const domestic = [43.3, 37.9, 43.7, 41.8, 50.3, 44.3];
        const fdi = [80.0, 84.6, 79.72, 116.7, 136.8, 120.3];
        const growthRate = [15.8, 5.8, -5.6, 34.3, 17.2, -12];

        // Tạo biểu đồ cột và đường
        Highcharts.chart('chart-container5', {
            chart: {
                type: 'column' // Loại biểu đồ: cột
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years5,
                title: {
                    text: 'Năm'
                }
            },
            yAxis: [{
                title: {
                    text: ' '
                }
            },
                {
                    title: {
                        text: ' ',
                        style: {
                            color: Highcharts.getOptions().colors[2] // Màu cho trục thứ 2 (đường)
                        }
                    },
                    labels: {
                        format: '{value}%' // Định dạng cho label trên trục thứ 2
                    },
                    opposite: true // Đặt trục thứ 2 bên phải
                }
            ],
            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },
            series: [{
                name: 'Trong nước',
                data: domestic,
                yAxis: 0, // Gắn với trục thứ 1
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột trong nước
            },
                {
                    name: 'FDI',
                    data: fdi,
                    yAxis: 0, // Gắn với trục thứ 1
                    color: 'rgba(255, 144, 14, 0.8)' // Màu của cột FDI
                },
                {
                    name: 'Mức độ tăng trưởng',
                    data: growthRate,
                    type: 'line', // Loại biểu đồ: đường
                    yAxis: 1, // Gắn với trục thứ 2
                    color: 'red' // Màu của đường
                }
            ]
        });
    </script>
    {{-- biểu đồ 7 --}}
    <script>
        // Dữ liệu FDI xuất siêu, trong nước nhập siêu và cả nước xuất siêu
        const years7 = ['2018', '2019', '2020', '2021', '2022', '2023 (6 tháng)'];
        const fdiExport = [32.8, 35.8, 34.6, 29.36, 41.9, 21.7];
        const domesticImport = [25.6, 25.9, 15.5, 25.36, 30.7, 9.8];
        const totalExport = [7.2, 9.9, 19.1, 4.0, 11.2, 12.8];

        // Tạo biểu đồ cột ngang cho từng loại dữ liệu
        Highcharts.chart('chart-container7', {
            chart: {
                type: 'bar' // Loại biểu đồ: cột ngang
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: years7,
                title: {
                    text: ' '
                }
            },
            yAxis: {
                title: {
                    text: ' '
                }
            },
            legend: {
                reversed: true // Đảo ngược thứ tự của legend
            },
            plotOptions: {
                series: {
                    stacking: 'normal' // Cột chồng lên nhau
                }
            },
            series: [{
                name: 'Cả nước xuất siêu',
                data: totalExport,
                color: 'rgba(124, 181, 236, 0.8)' // Màu của cột cả nước xuất siêu
            }]
        }, function (chart) {
            chart.addSeries({
                name: 'Khu vực trong nước nhập siêu',
                data: domesticImport,
                color: 'rgba(255, 144, 14, 0.8)' // Màu của cột khu vực trong nước nhập siêu
            });
            chart.addSeries({
                name: 'Khu vực FDI xuất siêu',
                data: fdiExport,
                color: 'rgba(186, 60, 161, 0.8)' // Màu của cột khu vực FDI xuất siêu
            });
        });
    </script>
    {{-- biểu đồ 1 --}}
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var data = [{
                name: '2012',
                xuat_sieu: 0.284,
                nhap_sieu: null
            },
                {
                    name: '2013',
                    xuat_sieu: 0.863,
                    nhap_sieu: null
                },
                {
                    name: '2014',
                    xuat_sieu: 2,
                    nhap_sieu: null
                },
                {
                    name: '2015',
                    xuat_sieu: 0,
                    nhap_sieu: 3.2
                },
                {
                    name: '2016',
                    xuat_sieu: 2.68,
                    nhap_sieu: null
                },
                {
                    name: '2017',
                    xuat_sieu: 2.7,
                    nhap_sieu: null
                },
                {
                    name: '2018',
                    xuat_sieu: 7.2,
                    nhap_sieu: null
                },
                {
                    name: '2019',
                    xuat_sieu: 9.9,
                    nhap_sieu: null
                },
                {
                    name: '2020',
                    xuat_sieu: 19.1,
                    nhap_sieu: null
                },
                {
                    name: '2021',
                    xuat_sieu: 4,
                    nhap_sieu: null
                },
                {
                    name: '2022',
                    xuat_sieu: 11.2,
                    nhap_sieu: null
                },
                {
                    name: '6/2023',
                    xuat_sieu: 12.8,
                    nhap_sieu: null
                }
            ];

            var categories = data.map(function (item) {
                return item.name;
            });

            var xuatSieuValues = data.map(function (item) {
                return item.xuat_sieu;
            });

            var nhapSieuValues = data.map(function (item) {
                return item.nhap_sieu;
            });

            Highcharts.chart('nhapsieu-container-data', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: categories,
                    title: {
                        text: ' '
                    }
                },
                yAxis: {
                    title: {
                        text: ' '
                    }
                },
                    @if(\Agent::isMobile())
                    series: [{
                        name: 'Xuất siêu',
                        data: xuatSieuValues,
                        color: 'blue', // Màu xanh cho dữ liệu xuất siêu
                        pointWidth: 20
                    },
                        {
                            name: 'Nhập siêu',
                            data: nhapSieuValues,
                            color: 'red',// Màu đỏ cho dữ liệu nhập siêu
                            pointWidth: 20
                        }
                    ]
                    @else
                    series: [{
                        name: 'Xuất siêu',
                        data: xuatSieuValues,
                        color: 'blue', // Màu xanh cho dữ liệu xuất siêu
                        pointWidth: 30
                    },
                        {
                            name: 'Nhập siêu',
                            data: nhapSieuValues,
                            color: 'red',// Màu đỏ cho dữ liệu nhập siêu
                            pointWidth: 30
                        }
                    ]

                    @endif

            });
        });
    </script>

    <script>
        var options = {
            title: {
                text: " "
            },
            data: [{
                type: "pie",
                startAngle: 45,
                showInLegend: "true",
                legendText: "{label}",
                indexLabelPlacement: "inside",
                indexLabel: " {y} %",
                yValueFormatString: "#,##0.#" % "",
                dataPoints: [{
                    label: "FDI",
                    y: 71.7
                },
                    {
                        label: "Trong nước",
                        y: 100 - 71.7
                    },

                ]
            }]
        };
        $("#pie-2018").CanvasJSChart(options);
    </script>

    <script>
        var options = {
            title: {
                text: " "
            },
            data: [{
                type: "pie",
                startAngle: 45,
                showInLegend: "true",
                legendText: "{label}",
                indexLabelPlacement: "inside",
                indexLabel: " {y} %",
                yValueFormatString: "#,##0.#" % "",
                toolTipContent: "",
                creditText: "",

                dataPoints: [{
                    label: "FDI",
                    y: 68.8
                },
                    {
                        label: "Trong nước",
                        y: 100 - 68.8
                    },

                ]
            }]
        };
        $("#pie-2019").CanvasJSChart(options);
    </script>
    <script>
        var options = {
            title: {
                text: " "
            },
            data: [{
                type: "pie",
                startAngle: 45,
                showInLegend: "true",
                indexLabelPlacement: "inside",
                legendText: "{label}",
                indexLabel: " {y} %",
                yValueFormatString: "#,##0.#" % "",
                dataPoints: [{
                    label: "FDI",
                    y: 72.2
                },
                    {
                        label: "Trong nước",

                        y: 100 - 72.2
                    },

                ]
            }]
        };
        $("#pie-2020").CanvasJSChart(options);
    </script>

    <script>
        var options = {
            title: {
                text: " "
            },
            data: [{
                type: "pie",
                startAngle: 45,
                showInLegend: "true",
                legendText: "{label}",
                indexLabel: "{y} %",
                indexLabelPlacement: "inside",
                yValueFormatString: "#,##0.#" % "",
                dataPoints: [{
                    label: "FDI",
                    y: 73.6
                },
                    {
                        label: "Trong nước",
                        y: 100 - 73.6
                    },

                ]
            }]
        };
        $("#pie-2021").CanvasJSChart(options);
    </script>

    <script>
        var options = {
            title: {
                text: " "
            },
            data: [{
                type: "pie",
                startAngle: 45,
                showInLegend: "true",
                legendText: "{label}",
                indexLabel: "{y} %",
                indexLabelPlacement: "inside",
                yValueFormatString: "#,##0.#" % "",
                dataPoints: [{
                    label: "FDI",
                    y: 74.4
                },
                    {
                        label: "Trong nước",
                        y: 100 - 74.4
                    },

                ]
            }]
        };
        $("#pie-2022").CanvasJSChart(options);
    </script>

    <script>
        var options = {
            title: {
                text: " "
            },
            data: [{
                type: "pie",
                startAngle: 45,
                showInLegend: "true",
                legendText: "{label}",
                indexLabelPlacement: "inside",
                indexLabel: "{y} %",
                yValueFormatString: "#,##0.#" % "",
                dataPoints: [{
                    label: "FDI",
                    y: 73.0
                },
                    {
                        label: "Trong nước",
                        y: 100 - 73.0
                    },

                ]
            }]
        };
        $("#pie-2023").CanvasJSChart(options);
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js"
            integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous">
    </script>
</body>

</html>
